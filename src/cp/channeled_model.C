#include "bbss_model.h"

#include <list>
#include <set>
#include <vector>

using namespace std;

/******************************
 MODEL POSTING
 ******************************/

void BBSSModel::post_channeling()
{
  /* channeling constraints: */
  /*
   * V... number of vehicles
   * S... number of stations
   *
   */
  
  /* 1: Channeling the predecessor/successor variables with the route variables
   *  succ[U]/pred[U] ~ route[K,V]
   *
   * forall v : 0..V-1
   *   forall k : 0.. K-1
   *   successor[route[k][v] + V] = route[k+1][v] + V
   *   predecessor[route[k][v] + V] = route[k-1][v] + V
   *
   */
  
  Matrix<IntVarArgs> route(route_v, K + 1, in.L_num);
  
  vector<Matrix<IntVarArgs>*> service_c(K + 1);
  for (int k = 0; k <= K; k++)
    service_c[k] = new Matrix<IntVarArgs>(service_v[k], in.L_num, in.V_num);
  
  const int max_vehicle_capacity = *(max_element(in.Z.begin(), in.Z.end()));
  const int max_station_capacity = *(max_element(in.C.begin(), in.C.end()));
  
  for (int k = 0; k < K; k++)
  {
    for(int v = 0; v < in.L_num; v++)
    {
      IntVar station1(*this, 0, (int)(U.size())), station2(*this, 0, (int)(U.size()));
      if (k > 0)
        rel(*this, (route(k, v) < in.V_num) >> (station1 == route(k, v) + regular_stations_start_index));
      else
        rel(*this, station1 == v);
      rel(*this, (route(k, v) < in.V_num && route(k + 1, v) < in.V_num) >> (station2 == route(k + 1, v) + regular_stations_start_index));
      rel(*this, (route(k, v) < in.V_num && route(k + 1, v) >= in.V_num) >> (station2 == regular_stations_end_index + v));
      // channeling succ and route variables
      element(*this, succ, station1, station2);
      // channeling service variables
      IntVar service1(*this, -min(max_vehicle_capacity, max_station_capacity), min(max_vehicle_capacity, max_station_capacity));
      rel(*this, (route(k, v) < in.V_num) >> (service1 == -sum(service_c[k]->col(v))));
      element(*this, service, station1, service1);
    }
  }
  for (int k = 0; k <= K; k++)
    delete service_c[k];
  
}


/******************************
 BRANCHING
 ******************************/

void BBSSModel::tree_search_branching_channeling()
{
  tree_search_branching_routing();
  tree_search_branching_step();
}


void BBSSModel::initial_solution_branching_channeling(unsigned long int restart)
{
  Rnd r(0U);
  r.time();
  Matrix<IntVarArgs> route(route_v, K + 1,  in.L_num);
  for(int l=0; l<in.L_num; l++) {
    branch(*this, route.row(l), INT_VAR_RND(r), INT_VAL_RND(r));
  }
  branch(*this, succ, INT_VAR_RND(r), INT_VAL_RND(r));
  branch(*this, deviation_from_target, INT_VAR_MAX_MAX(), INT_VAL_MIN());
  branch(*this, service, INT_VAR_NONE(), INT_VAL_MIN());
  
  for(int l=0; l<in.L_num; l++) {
    // search for instructions right after searching for a route for l
    // instruction variables
    IntVarArgs instr(0);
    for (int k = 1; k < K; k++) {
      Matrix<IntVarArgs> activity_at_k(activity_v[k], in.L_num, in.V_num);
      IntVarArgs x(activity_at_k.col(l));
      instr = instr+x;
    }
    branch(*this, instr, INT_VAR_DEGREE_MAX(), INT_VAL_MAX());
  }
  
  /*
   initial_solution_branching_routing(restart);
   initial_solution_branching_step(restart);
   */
}


void BBSSModel::post_aco_branching_channeling()
{
  ACOBrancher<BBSSModel>::post(*this);
  branch(*this, deviation_from_target, INT_VAR_NONE(), INT_VAL_MIN());
}


/******************************
 SUPPORT FOR LNS ENGINE
 ******************************/

unsigned int BBSSModel::relax_channeling(Space* neighbor, unsigned int free)
{
  Rnd r(0U);
  r.time();
  
  double rn = (double) r(RAND_MAX) / (double) RAND_MAX;
  
  if (rn < 0.5)
    return relax_step(neighbor, free);
  else
    return relax_routing(neighbor, free);
}

/******************************
 SUPPORT FOR ACO ENGINE
 ******************************/

const Gecode::IntVarArray& BBSSModel::vars_channeling() const
{
  return vars_step();
}