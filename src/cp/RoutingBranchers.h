//
//  RoutingBranchers.h
//  ACOCP
//
//  Created by Luca Di Gaspero on 19/02/13.
//  Copyright (c) 2013 Università degli Studi di Udine. All rights reserved.
//

#ifndef RoutingBranchers__
#define RoutingBranchers__

#include <gecode/search.hh>
#include <gecode/int.hh>

using namespace Gecode;

class BBSSModel;

/* Just a placeholder */

class RoutingBrancher : public Brancher
{
protected:
  /** Constructor. */
  RoutingBrancher(Home home) : Brancher(home) {}

  /** Copy constructor. */
  RoutingBrancher(Home home, bool share, RoutingBrancher& b) : Brancher(home, share, b) {}
};

class RoutingChoice : public Choice
{
public:
  enum BranchType { BRANCH_ON_SUCC = 0, BRANCH_ON_SERVICE, BRANCH_ON_DUMMY_VEHICLE};
  BranchType type;
  int station;
  int *values;
  int n_values;

  RoutingChoice(const RoutingBrancher& b, BranchType t, int s, const std::vector<int>& vals);
  /**  */
  virtual size_t size(void) const;
  /** Add choice to archive. */
  virtual void archive(Archive& e) const;
  ~RoutingChoice();
};

/** This brancher interleaves the assignment of service and succ variables (in this order). 
 *  Moreover, it assigns the succ variable of vehicles in a round-robin way (apart of dummy).
 *  The dummy vehicle assignment is a singleton atomic choice which will include all the remaining nodes
 *  once the full routes of all the other vehicles have been designed.
 *
 *
 *  TODO: verify whether this is a suitable choice when processing time is equal to zero, or rather it would be 
 *        better to design routes first and afterwards determine the operations.
 */

class RoutingInterleavingBrancher : public RoutingBrancher
{
public:
  
  /** Checks whether there is any station yet to be assigned, stores vector of alternatives based on domains. */
  virtual bool status(const Space& home) const;
  /** Generates a choice. */
  virtual Choice* choice(Space& home);
  
  /** Retrieves a choice from the archive. */
  virtual Choice* choice(const Space&, Archive& e);
  /** Constructor. */
  RoutingInterleavingBrancher(Home home);
  
  /** Copy constructor. */
  RoutingInterleavingBrancher(Home home, bool share, RoutingInterleavingBrancher& b);
  /** Clone support. */
  virtual Actor* copy(Space& home, bool share);
  
  /** Execute choice. */
  virtual ExecStatus commit(Space& home, const Choice& c, unsigned int a);
  /** Post brancher. */
  static void post(BBSSModel& home);

  void print(const Space& home, const Choice&	c, unsigned int a, std::ostream& o) const;

protected:
  int next_station_to_assign(const BBSSModel& home) const;
  ExecStatus fix_dummy_route(BBSSModel& b, const RoutingChoice& rc) const;
};


/** Post function. */

void routing_interleaving_brancher(BBSSModel& home)
{
  if (home.failed())
    return;
  RoutingInterleavingBrancher::post(home);
}


RoutingChoice::RoutingChoice(const RoutingBrancher& b, BranchType t, int s, const std::vector<int>& vals)
: Choice(b, (t == BRANCH_ON_DUMMY_VEHICLE) ? 1 : (unsigned int)vals.size()), type(t), station(s)
{
  if (t != BRANCH_ON_DUMMY_VEHICLE)
  {
    n_values = (int)vals.size();
    values = heap.alloc<int>(n_values);
    for (int i = 0; i < (int)vals.size(); i++)
      values[i] = vals[i];
  }
  else
  {
    // it should be meaningless, but if there's no value, the gecode system crashes
    n_values = 1;
    values = heap.alloc<int>(n_values);
    values[0] = station;
  }
  // in case of the singleton choice of branching on dummy vehicle, multiple variables (i.e., those of the left unserved stations) have to be assigned
  // and we handle this case in the commit() method of the brancher itself
}


/** Just for serialization */
size_t RoutingChoice::size(void) const
{
  return sizeof(RoutingChoice) + sizeof(int) * n_values;
}

/** Add choice to archive. */

void RoutingChoice::archive(Archive& e) const
{
  Choice::archive(e);
  e << type << station << n_values;
  for (int i = 0; i < n_values; i++)
    e << values[i];
}

/** Free allocated memory */
RoutingChoice::~RoutingChoice()
{
  heap.free<int>(values, n_values);
}

/**
 RoutingInterleavingBrancher methods definition
 */

int RoutingInterleavingBrancher::next_station_to_assign(const BBSSModel& r) const
{
  // This variant would interleave vehicles in route determination
	std::vector<std::pair<int, int> > path_length;
  std::vector<int> last_station(r.in.L_num);
  for (int current_vehicle = 0; current_vehicle < r.in.L_num; current_vehicle++)
  {		
    int current_station = r.starting_depot(current_vehicle);
		// follow the path to the first unassigned succ station value in the shortest vehicle route
    while (r.succ[current_station].assigned() && r.service[current_station].assigned())
      current_station = r.succ[current_station].val();		
		last_station[current_vehicle] = current_station;
		if (current_station != r.ending_depot(current_vehicle))
			path_length.push_back(std::make_pair(r.length[current_vehicle].min(), current_vehicle));
  }
			
	if (path_length.size() == 0)
    return r.starting_depot(r.dummy());
  else
  {
		// prefer the vehicle whose current route is the shortest
	  std::sort(path_length.begin(), path_length.end());
		return last_station[path_length[0].second];
  }
}


Choice* RoutingInterleavingBrancher::choice(Space &home)
{
  //const BBSSInstanceOptions& opt = dynamic_cast<BBSSModel&>(home).opt;
  //const BBSSInstance& in = r.getInstance();
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  std::vector<int> values(0);

  // set current station
  int station = next_station_to_assign(r);
  
	// there's a single possibility for the dummy vehicle, that is visit all remaining unassigned stations
	// this is handled by the commit function
  if (station == r.starting_depot(r.dummy()))
		return new RoutingChoice(*this, RoutingChoice::BRANCH_ON_DUMMY_VEHICLE, station, values);

	if (!r.service[station].assigned())
	{
		// this case arises when the service of the current vehicle has not been set yet
		std::vector<std::pair<int, int> > weighted_values;
		int current_bikes = r.nbBikes[station].val(), target_bikes = r.in.q[r.original_node(station)];
		for (Int::ViewValues<Int::IntView> j(r.service[station]); j(); ++j)
		{
			// in case of ties it should be preferred less activity
			int weight = (COST_SCALE * BBSStaubal()) * fabs(current_bikes - j.val() - target_bikes) + (COST_SCALE * BBSStauload()) * fabs(j.val());
			weighted_values.push_back(std::make_pair(weight, j.val()));
		}
		std::sort(weighted_values.begin(), weighted_values.end());
		for (unsigned int i = 0; i < weighted_values.size(); i++)
			values.push_back(weighted_values[i].second);

		return new RoutingChoice(*this, RoutingChoice::BRANCH_ON_SERVICE, station, values);
	}
	else // !r.succ[current_station[current_vehicle]].assigned()
	{
		// this case arises when the succ of the current vehicle has not been set yet
		std::vector<std::pair<int, int> > weighted_values;
		int current_load = r.load[station].val() + r.service[station].val();
		int current_vehicle = r.vehicle[station].val();		
		for (Int::ViewValues<Int::IntView> j(r.succ[station]); j(); ++j)
		{			
			int weight, next_station = j.val();
			// skip the value if next_station is a "revisit" and if the previous "revisits" have not been assigned yet
			if (r.is_revisit(next_station))
			{
				bool some_unassigned = false;
				for (int i = r.first_visit_to(r.original_node(next_station)); i < next_station; i++)
					if (!r.service[i].assigned())
					{
						some_unassigned = true;
						break;
					}
				if (some_unassigned)
					continue;
			}			

			int next_bikes = r.nbBikes[next_station].val(), target_bikes = r.in.q[r.original_node(next_station)], unbalance = next_bikes - target_bikes;
			int vehicle_capacity = r.in.Z[current_vehicle];
      
      // int lookahead_arrival_time_at_depot = r.arrival_time[station].val() + r.processing_time[station].val() + r.in.getTravelingTime(r.original_node(station), r.original_node(next_station), 0) + r.processing_time[next_station].max() + r.in.getTravelingTime(r.original_node(next_station), r.original_node(r.ending_depot(current_vehicle)), 0);
      
			if (unbalance > 0)
				// pickup station, i.e., service > 0
				weight = std::min(vehicle_capacity - current_load, unbalance);
			else
				// delivery station, i.e., service < 0
				weight = std::min(current_load, -unbalance);
      
      // if (lookahead_arrival_time_at_depot <= r.in.that[current_vehicle])
      
			weighted_values.push_back(std::make_pair((COST_SCALE * BBSStaubal()) * weight - (COST_SCALE * BBSStauload()) * weight - (COST_SCALE * BBSStauwork()) * r.in.getTravelingTime(r.original_node(station), r.original_node(next_station), 0), next_station));
		}
		std::sort(weighted_values.begin(), weighted_values.end(), std::greater<std::pair<int, int> >());
    for (unsigned int i = 0; i < weighted_values.size(); i++)
			values.push_back(weighted_values[i].second);
    
    
		return new RoutingChoice(*this, RoutingChoice::BRANCH_ON_SUCC, station, values);
	}  
}

void RoutingInterleavingBrancher::print(const Space& home, const Choice&	c, unsigned int a, std::ostream& os) const
{
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  const RoutingChoice& rc = static_cast<const RoutingChoice&>(c);

  switch (rc.type)
  {
    case RoutingChoice::BRANCH_ON_SUCC:
      os << "s[" << r.U[rc.station] << "/" << rc.station << "]=" << r.U[rc.values[a]] << "/" << rc.values[a];
      break;
    case RoutingChoice::BRANCH_ON_SERVICE:
      os << "o[" << r.U[rc.station] << "/" << rc.station << "]=" << rc.values[a];
      break;
    case RoutingChoice::BRANCH_ON_DUMMY_VEHICLE:
      os << "d";
      break;
      // FIXME: to handle this case
  }
}


bool RoutingInterleavingBrancher::status(const Space& home) const
{
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  for (int i = 0; i < r.succ.size(); i++)
    if (!r.succ[i].assigned() || !r.service[i].assigned())
      return true;

  return false;
}

/** Retrieves a choice from the archive. */

Choice* RoutingInterleavingBrancher::choice(const Space&, Archive& e)
{
  int station, n_values;
  RoutingChoice::BranchType t;
  int t_as_int;
  std::vector<int> values;
  e >> t_as_int >> station >> n_values;
  switch (t_as_int)
  {
    case 0: t = RoutingChoice::BRANCH_ON_SUCC; break;
    case 1: t = RoutingChoice::BRANCH_ON_SERVICE; break;
    case 2: t = RoutingChoice::BRANCH_ON_DUMMY_VEHICLE; break;
  }
  values.resize(n_values, -1);
  for (int i = 0; i < n_values; i++)
    e >> values[i];
  return new RoutingChoice(*this, t, station, values);
}

/** Constructor. */

RoutingInterleavingBrancher::RoutingInterleavingBrancher(Home home)
: RoutingBrancher(home)
{}

/** Copy constructor. */

RoutingInterleavingBrancher::RoutingInterleavingBrancher(Home home, bool share, RoutingInterleavingBrancher& b)
: RoutingBrancher(home, share, b)
{}

/** Clone support. */

Actor* RoutingInterleavingBrancher::copy(Space& home, bool share) {
  return new (home) RoutingInterleavingBrancher(home, share, *this);
}

/** Execute choice. */

ExecStatus RoutingInterleavingBrancher::commit(Space& home, const Choice& c, unsigned int a)
{
  BBSSModel& r = dynamic_cast<BBSSModel&>(home);
  const RoutingChoice& rc = static_cast<const RoutingChoice&>(c);
  /*
  std::cerr << "Committing choice " << a;
  switch(rc.type)
  {
    case RoutingChoice::BRANCH_ON_SUCC:
      std::cerr << " succ" << std::endl;
      break;
    case RoutingChoice::BRANCH_ON_SERVICE:
      std::cerr << " service" << std::endl;
      break;
    case RoutingChoice::BRANCH_ON_DUMMY_VEHICLE:
      std::cerr << " dummy" << std::endl;
      break;
  }*/
  
  // std::cerr << " (" << pv.pos << (a == 0 ? " == " : "!=") << pv.val << ") chosen ";

  switch (rc.type)
  {
    case RoutingChoice::BRANCH_ON_SUCC:
      //std::cerr << "succ[" << rc.station << "] = " << rc.values[a] << std::endl;
      return me_failed(Int::IntView(r.succ[rc.station]).eq(r, rc.values[a])) ? ES_FAILED : ES_OK;
    case RoutingChoice::BRANCH_ON_SERVICE:
      //std::cerr << "service[" << rc.station << "] (" << r.service[rc.station] << ") = " << rc.values[a] << std::endl;
      return me_failed(Int::IntView(r.service[rc.station]).eq(r, rc.values[a])) ? ES_FAILED : ES_OK;

    case RoutingChoice::BRANCH_ON_DUMMY_VEHICLE:
      return fix_dummy_route(r, rc);
  }
  GECODE_NEVER;
  return ES_FAILED;
}

ExecStatus RoutingInterleavingBrancher::fix_dummy_route(BBSSModel& r, const RoutingChoice& rc) const
{
  std::vector<int> left_unserved_stations;
  left_unserved_stations.push_back(r.starting_depot(r.dummy()));
  for (int s = r.regular_stations_start_index; s < r.regular_stations_end_index; s++)
    if (!r.succ[s].assigned())
      left_unserved_stations.push_back(s);
  for (std::vector<int>::const_iterator it = left_unserved_stations.begin(); it != left_unserved_stations.end() - 1; it++)
    if (me_failed(Int::IntView(r.succ[*it]).eq(r, *(it + 1))))
      return ES_FAILED;
  for (std::vector<int>::const_iterator it = left_unserved_stations.begin(); it != left_unserved_stations.end(); it++)
    if (me_failed(Int::IntView(r.service[*it]).eq(r, 0)))
      return ES_FAILED;
  for (std::vector<int>::const_iterator it = left_unserved_stations.begin(); it != left_unserved_stations.end(); it++)
    if (me_failed(Int::IntView(r.vehicle[*it]).eq(r, r.dummy())))
      return ES_FAILED;

  return ES_OK;
}

/** Post brancher. */

void RoutingInterleavingBrancher::post(BBSSModel& home)
{
  (void) new (home) RoutingInterleavingBrancher(home);
}


/********************************************
 * Safe two-steps Routing Brancher
 */


class TwoStepsRoutingChoice : public Choice
{
public:
  enum BranchType { FIX_ONE = 0, FIX_TWO, FIX_DUMMY_VEHICLE};
  BranchType type;
  int station;
  struct Values {
    int succ;
    int succ_succ;
    int service;
    int distance;
  };
  Values *values;
  int n_values;

  TwoStepsRoutingChoice(const Brancher& b, BranchType t, int s, const std::vector<Values>& vals);
  /**  */
  virtual size_t size(void) const;
  /** Add choice to archive. */
  virtual void archive(Archive& e) const;
  ~TwoStepsRoutingChoice();
};

bool compare_service(const TwoStepsRoutingChoice::Values& v1, const TwoStepsRoutingChoice::Values& v2)
{
  if (v1.service != v2.service)
    return v1.service > v2.service;
  else if (v1.succ != v2.succ)
    return v1.succ < v2.succ;
  else if (v1.succ_succ != v2.succ_succ)
    return v1.succ_succ < v2.succ_succ;
  else
    return false;
}

/** TODO: Describe it later
 */

class TwoStepsRoutingBrancher : public Brancher
{
public:

  /** Checks whether there is any station yet to be assigned, stores vector of alternatives based on domains. */
  virtual bool status(const Space& home) const;
  /** Generates a choice. */
  virtual Choice* choice(Space& home);

  /** Retrieves a choice from the archive. */
  virtual Choice* choice(const Space&, Archive& e);
  /** Constructor. */
  TwoStepsRoutingBrancher(Home home);

  /** Copy constructor. */
  TwoStepsRoutingBrancher(Home home, bool share, TwoStepsRoutingBrancher& b);
  /** Clone support. */
  virtual Actor* copy(Space& home, bool share);

  /** Execute choice. */
  virtual ExecStatus commit(Space& home, const Choice& c, unsigned int a);
  /** Post brancher. */
  static void post(BBSSModel& home);

  void print(const Space& home, const Choice&	c, unsigned int a, std::ostream& o) const;

protected:
  int next_station_to_assign(const BBSSModel& home) const;
  ExecStatus fix_dummy_route(BBSSModel& b, const TwoStepsRoutingChoice& rc) const;
};


/** Post function. */

void two_steps_routing_brancher(BBSSModel& home)
{
  if (home.failed())
    return;
  TwoStepsRoutingBrancher::post(home);
}


TwoStepsRoutingChoice::TwoStepsRoutingChoice(const Brancher& b, BranchType t, int s, const std::vector<Values>& vals)
: Choice(b, (t == FIX_DUMMY_VEHICLE) ? 1 : (unsigned int)vals.size()), type(t), station(s)
{
  if (t != FIX_DUMMY_VEHICLE)
  {
    n_values = (int)vals.size();
    values = heap.alloc<Values>(n_values);
    for (int i = 0; i < (int)vals.size(); i++)
      values[i] = vals[i];
  }
  else
  {
    // it should be meaningless, but if there's no value, the gecode system crashes
    n_values = 1;
    values = heap.alloc<Values>(n_values);
  }
  // in case of the singleton choice of branching on dummy vehicle, multiple variables (i.e., those of the left unserved stations) have to be assigned
  // and we handle this case in the commit() method of the brancher itself
}


/** Just for serialization */
size_t TwoStepsRoutingChoice::size(void) const
{
  return sizeof(TwoStepsRoutingChoice) + sizeof(Values) * n_values;
}

/** Add choice to archive. */

void TwoStepsRoutingChoice::archive(Archive& e) const
{
  Choice::archive(e);
  e << type << station << n_values;
  for (int i = 0; i < n_values; i++)
    e << values[i].succ << values[i].succ_succ << values[i].service;
}

/** Free allocated memory */
TwoStepsRoutingChoice::~TwoStepsRoutingChoice()
{
  heap.free<Values>(values, n_values);
}

/**
 RoutingInterleavingBrancher methods definition
 */

int TwoStepsRoutingBrancher::next_station_to_assign(const BBSSModel& r) const
{
  // This variant would interleave vehicles in route determination
	std::vector<std::pair<int, int> > path_length;
  std::vector<int> last_station(r.in.L_num);
  for (int current_vehicle = 0; current_vehicle < r.in.L_num; current_vehicle++)
  {
    int current_station = r.starting_depot(current_vehicle);
		// follow the path to the first unassigned succ station value in the shortest vehicle route
    while (r.succ[current_station].assigned() && r.service[current_station].assigned())
      current_station = r.succ[current_station].val();
		last_station[current_vehicle] = current_station;
		if (current_station != r.ending_depot(current_vehicle))
			path_length.push_back(std::make_pair(r.length[current_vehicle].min(), current_vehicle));
  }

	if (path_length.size() == 0)
    return r.starting_depot(r.dummy());
  else
  {
		// prefer the vehicle whose current route is the shortest
	  std::sort(path_length.begin(), path_length.end());
		return last_station[path_length[0].second];
  }
}


Choice* TwoStepsRoutingBrancher::choice(Space &home)
{
  //const BBSSInstanceOptions& opt = dynamic_cast<BBSSModel&>(home).opt;
  //const BBSSInstance& in = r.getInstance();
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  std::vector<TwoStepsRoutingChoice::Values> values;
  
  // set current station
  int station = next_station_to_assign(r);
 
	// there's a single possibility for the dummy vehicle, that is visit all remaining unassigned stations
	// this is handled by the commit function
  if (station == r.starting_depot(r.dummy()))
		return new TwoStepsRoutingChoice(*this, TwoStepsRoutingChoice::FIX_DUMMY_VEHICLE, station, values);

  int current_load = r.load[r.pred[station].val()].val();
  if (current_load > 0) // the succ of station should be a sink
  {
    for (Int::ViewValues<Int::IntView> j(r.succ[station]); j(); ++j)
    {
      if (r.service[j.val()].in(-current_load)) // the succ (j.val()) allows to go to zero load
      {
        TwoStepsRoutingChoice::Values v;
        v.succ = j.val();
        v.succ_succ = -1;
        v.service = -current_load;
        v.distance = r.in.getTravelingTime(r.original_node(station), r.original_node(j.val()), 0);
        values.push_back(v);
      }
    }
    // TODO: sort the values according to minimum "waste"
    return new TwoStepsRoutingChoice(*this, TwoStepsRoutingChoice::FIX_ONE, station, values);
  }
  else // the succ of station should be a source and the succ_succ should be a suitable sink or the vehicle should go back to the depot
  {
    std::vector<std::pair<int, TwoStepsRoutingChoice::Values> > weighted_values;
    for (Int::ViewValues<Int::IntView> j(r.succ[station]); j(); ++j)
    {
      
      if (r.service[j.val()].min() < 0) // skip sinks
        continue;
      
      
      if (!r.nbBikes[j.val()].assigned())
        continue;
      
      if (r.ending_depot(r.vehicle[station].val()) == j.val())  // skip ending depot (here, it will be included later)
        continue;
      
      for (Int::ViewValues<Int::IntView> k(r.succ[j.val()]); k(); ++k)
      {
        if (r.service[j.val()].min() > 0) // skip sources
          continue;
        
        if (r.ending_depot(r.vehicle[station].val()) == k.val()) // skip ending depot
          continue;
        TwoStepsRoutingChoice::Values v;
        v.succ = j.val();
        v.succ_succ = k.val();
        v.service = std::min(r.nbBikes[j.val()].val() - r.in.q[r.original_node(j.val())], -r.service[k.val()].min());
        v.distance = r.in.getTravelingTime(r.original_node(station), r.original_node(j.val()), 0) + r.in.getTravelingTime(r.original_node(j.val()), r.original_node(k.val()), 0);
        values.push_back(v);
      }
    }
    std::sort(values.begin(), values.end(), &compare_service);
    
    // at least go directly to the depot
    TwoStepsRoutingChoice::Values v;
    v.succ = r.ending_depot(r.vehicle[station].val());
    v.succ_succ = -1;
    v.service = 0;
    v.distance = 0;
    values.push_back(v);
    return new TwoStepsRoutingChoice(*this, TwoStepsRoutingChoice::FIX_TWO, station, values);
  }
}

void TwoStepsRoutingBrancher::print(const Space& home, const Choice&	c, unsigned int a, std::ostream& os) const
{
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  const TwoStepsRoutingChoice& rc = static_cast<const TwoStepsRoutingChoice&>(c);

  switch (rc.type)
  {
    case TwoStepsRoutingChoice::FIX_ONE:
      os << "f1(" << rc.n_values << ") s[" << r.U[rc.station] << "/" << rc.station << "]=" << r.U[rc.values[a].succ] << "/" << rc.values[a].succ << "," << rc.values[a].service;
      break;
    case TwoStepsRoutingChoice::FIX_TWO:
      os << "f2(" << rc.n_values << ") s[" << r.U[rc.station] << "/" << rc.station << "]=" << r.U[rc.values[a].succ] << "/" << rc.values[a].succ << ", s[" << r.U[rc.values[a].succ] << "/" << rc.values[a].succ << "]=" << (rc.values[a].succ_succ >= 0 ? r.U[rc.values[a].succ_succ] : rc.values[a].succ_succ) << "/" << rc.values[a].succ_succ << "," << rc.values[a].service;
      break;
    case TwoStepsRoutingChoice::FIX_DUMMY_VEHICLE:
      os << "d";
      break;
      // FIXME: to handle this case
  }
}


bool TwoStepsRoutingBrancher::status(const Space& home) const
{
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  for (int i = 0; i < r.succ.size(); i++)
    if (!r.succ[i].assigned()) // || !r.service[i].assigned())
      return true;

  return false;
}

/** Retrieves a choice from the archive. */

Choice* TwoStepsRoutingBrancher::choice(const Space&, Archive& e)
{
  int station, n_values;
  TwoStepsRoutingChoice::BranchType t;
  int t_as_int;
  std::vector<TwoStepsRoutingChoice::Values> values;
  e >> t_as_int >> station >> n_values;
  switch (t_as_int)
  {
    case 0: t = TwoStepsRoutingChoice::FIX_ONE; break;
    case 1: t = TwoStepsRoutingChoice::FIX_TWO; break;
    case 2: t = TwoStepsRoutingChoice::FIX_DUMMY_VEHICLE; break;
  }
  values.resize(n_values);
  for (int i = 0; i < n_values; i++)
    e >> values[i].succ >> values[i].succ_succ >> values[i].service;
  return new TwoStepsRoutingChoice(*this, t, station, values);
}

/** Constructor. */

TwoStepsRoutingBrancher::TwoStepsRoutingBrancher(Home home)
: Brancher(home)
{}

/** Copy constructor. */

TwoStepsRoutingBrancher::TwoStepsRoutingBrancher(Home home, bool share, TwoStepsRoutingBrancher& b)
: Brancher(home, share, b)
{}

/** Clone support. */

Actor* TwoStepsRoutingBrancher::copy(Space& home, bool share) {
  return new (home) TwoStepsRoutingBrancher(home, share, *this);
}

/** Execute choice. */

ExecStatus TwoStepsRoutingBrancher::commit(Space& home, const Choice& c, unsigned int a)
{
  BBSSModel& r = dynamic_cast<BBSSModel&>(home);
  const TwoStepsRoutingChoice& rc = static_cast<const TwoStepsRoutingChoice&>(c);
  bool failed;

  switch (rc.type)
  {
    case TwoStepsRoutingChoice::FIX_ONE:
    {
      failed = me_failed(Int::IntView(r.succ[rc.station]).eq(r, rc.values[a].succ));
      if (!failed)
        failed = me_failed(Int::IntView(r.service[rc.values[a].succ]).eq(r, rc.values[a].service));
      return (failed ? ES_FAILED : ES_OK);
    }
    case TwoStepsRoutingChoice::FIX_TWO:
    {
      failed = me_failed(Int::IntView(r.succ[rc.station]).eq(r, rc.values[a].succ));
      if (!failed && rc.values[a].succ_succ >= 0)
        failed = me_failed(Int::IntView(r.succ[rc.values[a].succ]).eq(r, rc.values[a].succ_succ));
      if (!failed)
        failed = me_failed(Int::IntView(r.service[rc.values[a].succ]).eq(r, rc.values[a].service));
      if (!failed && rc.values[a].succ_succ >= 0)
        failed = me_failed(Int::IntView(r.service[rc.values[a].succ_succ]).eq(r, -rc.values[a].service));
      return (failed ? ES_FAILED : ES_OK);
    }

    case TwoStepsRoutingChoice::FIX_DUMMY_VEHICLE:
      return fix_dummy_route(r, rc);
  }
  GECODE_NEVER;
  return ES_FAILED;
}

ExecStatus TwoStepsRoutingBrancher::fix_dummy_route(BBSSModel& r, const TwoStepsRoutingChoice& rc) const
{
  
  std::vector<int> left_unserved_stations;
  left_unserved_stations.push_back(r.starting_depot(r.dummy()));
  for (int s = r.regular_stations_start_index; s < r.regular_stations_end_index; s++)
    if (!r.succ[s].assigned())
      left_unserved_stations.push_back(s);
  for (std::vector<int>::const_iterator it = left_unserved_stations.begin(); it != left_unserved_stations.end() - 1; it++)
    if (me_failed(Int::IntView(r.succ[*it]).eq(r, *(it + 1))))
      return ES_FAILED;
  for (std::vector<int>::const_iterator it = left_unserved_stations.begin(); it != left_unserved_stations.end(); it++)
    if (me_failed(Int::IntView(r.service[*it]).eq(r, 0)))
      return ES_FAILED;
  for (std::vector<int>::const_iterator it = left_unserved_stations.begin(); it != left_unserved_stations.end(); it++)
    if (me_failed(Int::IntView(r.vehicle[*it]).eq(r, r.dummy())))
      return ES_FAILED;

  return ES_OK;
}

/** Post brancher. */

void TwoStepsRoutingBrancher::post(BBSSModel& home)
{
  (void) new (home) TwoStepsRoutingBrancher(home);
}


#endif /* defined(RoutingBrancher__) */
