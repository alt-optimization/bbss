#include "bbss_model.h"

#include <list>
#include <set>
#include <vector>

// Custom branchers
#include "StepBrancher.h"

using namespace std;

/******************************
 MODEL POSTING
 ******************************/

void BBSSModel::post_step()
{  
  /** ===== Constants, preprocessing ===== */
  
  // Useful constants
  const int max_vehicle_capacity = *(max_element(in.Z.begin(), in.Z.end()));
  const int max_station_capacity = *(max_element(in.C.begin(), in.C.end()));

  /** =====  Decision variables: route, loading/unloading instructions ===== */
  
  /** Route variables: a route consists of K+1 steps for each vehicle, step 0
   is from the starting depot to the first node, step K leads (at the latest)
   to the final depot. */
  route_v = IntVarArray(*this, (K + 1) * in.L_num, 0, in.V0_num - 1);
  
  // Superimpose matrix (index by vehicles)
  Matrix<IntVarArgs> route(route_v, K + 1, in.L_num);
  
  /** Loading instructions: for each step, tell what service will be performed at
   a node from the perspective of a vehicle. Note that positive values mean loading
   the truck, negative ones mean loading the station (unlike int the ROUTING model).
   
   So service is a 3D matrix (first index is step, second index is vehicle, third
   index is node).
   */
  service_v.resize(K + 1);
  std::vector<Matrix<IntVarArgs>*> service(K + 1);
  
  // Absolute value of service (as in ROUTING model)
  activity_v.resize(K + 1);
  std::vector<Matrix<IntVarArgs>*> activity(K + 1);
  
  for (int k = 0; k <= K; k++)
  {
    // Definitive upper bound for service and activity
    const int upper_bound = min(max_vehicle_capacity, max_station_capacity);
    
    // Service variables as vectors of vectors
    service_v[k] = IntVarArray(*this, in.L_num * in.V_num, -upper_bound, upper_bound);
    activity_v[k] = IntVarArray(*this, in.L_num * in.V_num, 0, upper_bound);
    
    for (int i = 0; i < in.L_num * in.V_num; i++)
      rel(*this, activity_v[k][i] == abs(service_v[k][i]));
    
    // Service variables as vectors of matrices
    service[k] = new Matrix<IntVarArgs>(service_v[k], in.L_num, in.V_num);
    activity[k] = new Matrix<IntVarArgs>(activity_v[k], in.L_num, in.V_num);
  }
  
  /** =====  Decision variables: time of arrival to station ===== */
  
  // 2D matrix (step, vehicle), since station is fixed by step
  if (!opt.full_balancing())
    time_v = IntVarArray(*this, (K + 1) * in.L_num, 0, in.that_max);
  else
    time_v = IntVarArray(*this, (K + 1) * in.L_num, 0, Int::Limits::max);
  
  // As usual, matrix is superimposed
  Matrix<IntVarArgs> time(time_v, K + 1, in.L_num);
  
  /** =====  Auxiliary variables: load at stations, etc. ===== */
  
  // Load of truck after performing step k
  loadV_v = IntVarArray(*this, (K + 1) * in.L_num, 0, max_vehicle_capacity);
  Matrix<IntVarArgs> loadV(loadV_v, K + 1, in.L_num);

  // Variable loading time after performing step k
  IntVarArgs loadTime_v(*this, (K + 1) * in.L_num, 0, in.that_max);
  Matrix<IntVarArgs> loadTime(loadTime_v, K + 1, in.L_num);
  
  // Number of bikes at the station after performing step k
  nbBikes_v = IntVarArray(*this, (K + 1) * in.V0_num, 0, max_station_capacity);
  Matrix<IntVarArgs> nbBikes(nbBikes_v, K + 1, in.V0_num);
  
  /** =====  Domain constraints ===== */
  
  // On service/activity
  for (int k = 1; k < K; k++)
    for (int l = 0; l < in.L_num; l++)
      for (int v = 0; v < in.V_num; v++)
      {
        int upper_bound = min(in.Z[l], in.C[v]);
        dom(*this, (*service[k])(l, v), -upper_bound, upper_bound);
        dom(*this, (*activity[k])(l, v), 0, upper_bound);
      }
  
  // On truck load
  for (int k = 0; k <= K; k++)
    for (int l = 0; l < in.L_num; l++)
      dom(*this, loadV(k, l), 0, in.Z[l]);
  
  // On station load
  for (int k = 0; k <= K; k++)
    for (int v = 0; v < in.V_num; v++)
      dom(*this, nbBikes(k, v), 0, in.C[v]);
  
  // On depots
  for (int k = 0; k <= K; k++)
    for (int v = in.V_num; v < in.V0_num; v++)
      dom(*this, nbBikes(k, v), 0, 0);
  
  /** =====  Initialization constraints ===== */
  for (int l = 0; l < in.L_num; l++)
  {
    // ESSENTIAL: routes must start and end at depot
    dom(*this, route(0, l), in.V_num + in.s[l], in.V_num + in.s[l]);
    dom(*this, route(K, l), in.V_num + in.d[l], in.V_num + in.d[l]);
  }
  
  // No activity/service at steps 0 and K
  dom(*this, activity_v[0], 0, 0);
  dom(*this, activity_v[K], 0, 0);
  dom(*this, service_v[0], 0, 0);
  dom(*this, service_v[K], 0, 0);
  
  /** TODO: check if there's a constraint stating that service and
      activity must be zero even at steps != K but that are already depots
   */
  
  // Load of all vehicles at step 0 is initial load
  for (int l = 0; l < in.L_num; l++)
    rel(*this, loadV(0, l) == in.bhat[l]);
  
  // Initial number of bikes at the station comes from instance
  for (int v = 0; v < in.V_num; v++)
    rel(*this, nbBikes(0, v) == in.p[v]);
  
  // Time starts from 0 at step 0
  for (int l = 0; l < in.L_num; l++)
    dom(*this, time(0, l), 0, 0);
  
  // Loading time is 0 at step 0 and at step K
  for (int l = 0; l < in.L_num; l++)
  {
    dom(*this, loadTime(0, l), 0, 0);
    dom(*this, loadTime(K, l), 0, 0);
  }
  
  /** =====  Consistency constraints ===== */
  
  // NOTE: GECODE matrices are indexed by (column, row)
  
  // Vehicle l at step k (k != 0, K) may only add or remove bikes at most at one station
  // Actually: the number of stations with activity == 0 for a vehicle must be at least #nodes - 1
  for (int k = 1; k < K; k++)
    for (int l = 0; l < in.L_num; l++)
      // ESSENTIAL1
      atleast(*this, activity[k]->col(l), 0, in.V_num - 1);
  
  /** Time of arrival of a vehicle at the next station must be (non strictly) increasing
   to allow ending the tours before doing K steps (sequence of depots at the end of the route have constant time)
   
   TODO: maybe by rearranging the indexes (or using a MatrixView) you can use the global
   constraint "precede" implemented in Gecode.
   */
  for (int k = 0; k < K; k++)
    for (int l = 0; l < in.L_num; l++)
      //  ESSENTIAL2
      rel(*this, time(k, l) <= time(k + 1, l));
  
  /** =====  Action constraints ===== */
  
  //#ifdef ALLOW_MONOTONICITY
  if(opt.monotonicity())
  {
    for (int k = 1; k < K; k++)
      for (int l = 0; l < in.L_num; l++)
      {
        for (int v = 0; v < in.V_num; v++)
        {
          //ESSENTIAL3
          if (in.p[v] > in.q[v])
            rel(*this, (*service[k])(l, v) >= 0);
          else
            rel(*this, (*service[k])(l, v) <= 0);
        }
      }
  }
  //#endif
  
  // Update vehicle load
  for (int k = 0; k < K; k++)
    for (int l = 0; l < in.L_num; l++)
      // ESSENTIAL4
      rel(*this, loadV(k + 1, l) == loadV(k, l) + sum(service[k + 1]->col(l)));
  

  // loading time update
  // Andrea: they removed that value from the instance
  IntArgs tload(in.V_num);
  for (int v = 0; v < in.V_num; v++)
    tload[v] = opt.loading_time();

  // ESSENTIAL5
  // computes the processing time at step k for vehicle l
  for (int k = 1; k < K; k++)
    for (int l = 0; l < in.L_num; l++)
      rel(*this, loadTime(k, l) == sum(tload, activity[k]->col(l)));
  
  // Update number of bikes in the stations
  for (int k = 0; k < K; k++)
    for (int v = 0; v < in.V_num; v++)
      // ESSENTIAL6
      rel(*this, nbBikes(k + 1, v) == nbBikes(k, v) - sum(service[k + 1]->row(v)));
  
  // Update time
  IntArgs tt_v(in.V0_num * in.V0_num);
  
  // travel time matrix
  Matrix<IntArgs> tt(tt_v, in.V0_num, in.V0_num);
  for (int v = 0; v < in.V0_num; v++)
    for (int w = 0; w < in.V0_num; w++)
      tt(v, w) = in.getTravelingTime(v, w, 0);
  
  for (int k = 0; k < K; k++)
    for (int l = 0; l < in.L_num; l++)
    {
      IntVar tt_element(*this, 0, in.that_max);
      element(*this, tt, route(k, l), route(k + 1, l), tt_element);
      //ESSENTIAL7
      // without waiting time, TODO: waiting time can be introduced by using >= instead of ==
      rel(*this, time(k + 1, l) == time(k, l) + loadTime(k, l) + tt_element);
    }
  
  
  // ================= general constraints ==================================
  
  // TODO: post the constraint that
  // no pair of vehicles is at the same station during the same time interval
  
  // ESSENTIAL 8 (time constraint)
  // LUCA: no two vehicles at the same station at the same time
  for (int k1=1; k1<K-1; k1++)
    for (int k2=1; k2<K-1; k2++)
      for (int l1=0; l1<in.L_num; l1++)
        for (int l2=0; l2<in.L_num; l2++)
          if (l1 != l2)
            // TODO: possibly they can become scheduling constraints (e.g., through unary)
            // vehicles l1 and l2 are at the same station (not a depot) at steps k1 and k2 respectively
            // therefore, their services must not overlap on that node
            rel(*this, (route(k1, l1) == route(k2, l2) && route(k1, l1) < in.V_num) >> (time(k1, l1) + loadTime(k1, l1) <= time(k2, l2) || time(k2, l2) + loadTime(k2, l2) <= time(k1, l1)));

  
  // LUCA: I modified this to allow shorter routes
  IntVarArgs visits_to_node(*this, in.V0_num, 0, (K + 1) * in.L_num);
  count(*this, route_v, visits_to_node);
  for (int v = 0; v < in.V_num; v++)
    dom(*this, visits_to_node[v], 0, opt.revisits());

  // binding between route and service
  // if a node is visited in the route, the activity at the node should not be 0
  for (int k = 1; k < K; k++)
    for (int l = 0; l < in.L_num; l++)
      for (int v = 0; v < in.V_num; v++)
      {
        // route(k, l) == v <=> activity(k, l, v) > 0
        rel(*this, ((*activity[k])(l, v) > 0) == (route(k, l) == v));
      }
  
  // ESSENTIAL: enforces the routes to follow the specific pattern (start_depot, regular_node, ..., regular_node, end_depot, end_depot, ..., end_depot)
  for (int l = 0; l < in.L_num; l++)
  {
    IntArgs non_depots;
    for (int v = 0; v < in.V_num; v++)
      non_depots << v;
    REG r = REG(in.V_num + in.s[l]) + *REG(non_depots) + *REG(in.V_num + in.d[l]);
    DFA d(r);

    extensional(*this, route.row(l), d, opt.icl());
  }

  // no two consecutive visits to the same node are allowed by the same vehicle
  for (int k = 1; k < K; k++)
    for (int l = 0; l < in.L_num; l++)
      for (int v = 0; v < in.V_num; v++)
        rel(*this, (route(k, l) == v) >> (route(k + 1, l) != v));
  
  
  if (!opt.allow_load_at_final_depot())
    for (int l = 0; l < in.L_num; l++)
      rel(*this, loadV(K, l) == 0);

  // cost binding
  // TODO: this is just for the static case (i.e., after the whole number of steps has been made)
  
  IntVarArgs deviation_from_target;
  if (!opt.quadratic_deviation())
    deviation_from_target = IntVarArgs(*this, in.V_num, 0, max_station_capacity);
  else
    deviation_from_target = IntVarArgs(*this, in.V_num, 0, max_station_capacity * max_station_capacity);
  
  for (int v = 0; v < in.V_num; v++)
  {
    if (!opt.quadratic_deviation())
      rel(*this, deviation_from_target[v] == abs(nbBikes(K, v) - in.q[v]));
    else
      rel(*this, deviation_from_target[v] == sqr(nbBikes(K, v) - in.q[v]));
  }

  // Total activities at a step k
  IntVarArgs total_activities_at_k(*this, (K + 1), 0, in.L_num * max_vehicle_capacity);

  for (int k = 0; k <= K; k++)
    total_activities_at_k[k] = expr(*this, sum(activity_v[k]));
  

  if (!channeled(opt.model()))
    cost_deviation = expr(*this, sum(deviation_from_target));
  else
    rel(*this, cost_deviation == sum(deviation_from_target));
  
  if (!channeled(opt.model()))
    cost_load = expr(*this, sum(total_activities_at_k));
  else
    rel(*this, cost_load == sum(total_activities_at_k));

  if (!channeled(opt.model()))
    cost_work = expr(*this, sum(time.col(K)));
  else
    rel(*this, cost_work == sum(time.col(K)));

  if (!channeled(opt.model()))
    cost_var = expr(*this, (int)(COST_SCALE * BBSStaubal()) * cost_deviation + (int)(COST_SCALE * BBSStauload()) * cost_load + (int)(COST_SCALE * BBSStauwork()) * cost_work);
  else
    rel(*this, cost_var == (int)(COST_SCALE * BBSStaubal()) * cost_deviation + (int)(COST_SCALE * BBSStauload()) * cost_load + (int)(COST_SCALE * BBSStauwork()) * cost_work);

  for (int k = 0; k <= K; k++)
  {
    delete service[k];
    delete activity[k];
  }
  
  t.start();
  last_print_time = 0.0;
  cumulated_time = 0.0;
  
}

/******************************
 SPACE UPDATING
 ******************************/

void BBSSModel::update_step(bool share, BBSSModel& s)
{
  route_v.update(*this, share, s.route_v);
  nbBikes_v.update(*this, share, s.nbBikes_v);
  service_v.resize(K + 1);
  
  for (int k = 0; k <= K; k++)
    service_v[k].update(*this, share, s.service_v[k]);
  activity_v.resize(K + 1);
  for (int k = 0; k <= K; k++)
    activity_v[k].update(*this, share, s.activity_v[k]);
  cost_var.update(*this, share, s.cost_var);
  cost_deviation.update(*this, share, s.cost_deviation);
  cost_deviation.update(*this, share, s.cost_deviation);
  cost_load.update(*this, share, s.cost_load);
  cost_work.update(*this, share, s.cost_work);
  time_v.update(*this, share, s.time_v);
  loadV_v.update(*this, share, s.loadV_v);
  return;
}

/******************************
 BRANCHING
 ******************************/

void BBSSModel::tree_search_branching_step()
{
  if (smart_branching(opt.branching()))
    step_brancher(*this);
  else if (two_step_branching(opt.branching()))
  {
    two_steps_step_brancher(*this);
    branch(*this, deviation_from_target, INT_VAR_NONE(), INT_VAL_MIN());
  }
  else
    ; // decide the default branching
}

void BBSSModel::initial_solution_branching_step(unsigned long int restart)
{
  if (restart == 0)
  {
    if (!opt.allow_load_at_final_depot() && opt.monotonicity())
    {
      // Fail-safe routing
      two_steps_step_brancher(*this);
      
      // Fix pending services
      branch(*this, deviation_from_target, INT_VAR_NONE(), INT_VAL_MIN());
    }
    else
    {
      tree_search_branching_step();
    }
  }
  else
  {
    Rnd r(0U);
    r.time();
    Matrix<IntVarArgs> route(route_v, K + 1, in.L_num);
    for (int k = 1; k < K; k++)
      branch(*this, route.col(k), INT_VAR_DEGREE_MAX(), INT_VAL_RND(r));
    
    for (int k = 1; k < K; k++)
      branch(*this, activity_v[k], INT_VAR_DEGREE_MAX(), INT_VAL_MAX());
  }
  
}


void BBSSModel::neighborhood_branching_step()
{
  tree_search_branching();

  /*
  if (!smart_branching(opt.branching()))
  {
    Matrix<IntVarArgs> route(route_v, K + 1, in.L_num);
    for(int l=0; l<in.L_num; l++) {
      branch(*this, route.row(l), INT_VAR_SIZE_MIN(), INT_VAL_MIN());
      
      // search for instructions right after searching for a route for l
      // instruction variables
      IntVarArgs instr(0);
      for (int k = 1; k < K; k++) {
        Matrix<IntVarArgs> activity_at_k(activity_v[k], in.L_num, in.V_num);
        IntVarArgs x(activity_at_k.col(l));
        instr = instr+x;
      }
      branch(*this, instr, INT_VAR_DEGREE_MAX(), INT_VAL_MAX());
    }
  }
  else
  {
    step_brancher(*this);
  }*/
}

void BBSSModel::post_aco_branching_step()
{
  ACOBrancher<BBSSModel>::post(*this);
  branch(*this, deviation_from_target, INT_VAR_NONE(), INT_VAL_MIN());
}



/******************************
 PRINTING
 ******************************/

void BBSSModel::print_step(ostream& os) const
{
  
  print_json(os);
  
  return;
  
  
#if PRINT_CP_VARIABLES
  os << "Step Model Output ====================================" << endl;
  bool assigned = true;
  for (int k = 0; k <= K; k++)
    assigned &= service_v[k].assigned();
    Matrix<IntVarArgs> route(route_v, K + 1, in.L_num);
    vector<Matrix<IntVarArgs>*> service(K + 1);
    // FIXME: smart pointers should be used instead
    for (int k = 0; k <= K; k++)
      service[k] = new Matrix<IntVarArgs>(service_v[k], in.L_num, in.V_num);
      Matrix<IntVarArgs> nbBikes(nbBikes_v, K + 1, in.V0_num);
      Matrix<IntVarArgs> time(time_v, K + 1, in.L_num);
      Matrix<IntVarArgs> loadV(loadV_v, K + 1, in.L_num);
#if VERBOSE_PRINT
      os << "Route: " << endl << route << endl;
  os << "Number of bikes: " << endl << nbBikes << endl;
  for (int k = 0; k<= K; k++)
    os << "Service[" << k << "]: " << endl << (*service[k]) << endl;
  os << "Load: " << endl << loadV << endl;
#endif
  if (!assigned || !nbBikes_v.assigned() || !route_v.assigned())
    return;
  
  os << "Paths for vehicles: " << endl;
  for (int l = 0; l < in.L_num; l++)
  {
    os << "Path[" << l << "]: ";
    for (int k = 0; k < K; k++)
      os << route(k, l) << "@" << time(k, l) << "-->";
    os << route(K, l) << "@" << time(K, l) << endl;
    os << "Service[" << l << "]: ";
    int node;
    for (int k = 1; k < K; k++)
    {
      node = route(k, l).val();
      if (node == in.V_num + in.d[l])
        break;
      os <<  (*service[k])(l, node) << "/" << nbBikes(k, node) << " ";
    }
    os << endl;
  }
  os << "Initial status: " << nbBikes.col(0) << endl;
  os << "Final  status: " << nbBikes.col(K) << endl;
  IntArgs target(in.q);
  os << "Target status: " << target << endl;
  int deviation = 0;
  int initialDeviation = 0;
  os << "Initial Deviation    : {";
  for (int v = 0; v < in.V_num - 1; v++)
  {
    os << (in.p[v] - in.q[v]) << ", ";
  }
  os << (in.p[in.V_num - 1] - in.q[in.V_num - 1]) << "}" << endl;
  os << "Deviation    : {";
  for (int v = 0; v < in.V_num - 1; v++)
  {
    os << (nbBikes(K, v).val() - in.q[v]) << ", ";
    deviation += abs(nbBikes(K, v).val() - in.q[v]);
    initialDeviation += abs(in.p[v] - in.q[v]);
  }
  os << (nbBikes(K, in.V_num - 1).val() - in.q[in.V_num - 1]) << "}" << endl;
  deviation += abs(nbBikes(K, in.V_num - 1).val() - in.q[in.V_num - 1]);
  float improvement = ((float) (initialDeviation - deviation))/initialDeviation;
  os << "Total deviation from target: " << deviation <<
  " (initial deviation: " << initialDeviation << ")" << endl;
  os << "Improvement: " << improvement << endl;
  os << "Cost: " << cost_var << endl;
  // FIXME: smart pointers should be used instead
  for (int k = 0; k <= K; k++)
    delete service[k];
#endif
  BBSSSolution sol = getSolution();
  os << "CP step model on file " << opt.instance() <<  endl;
  sol.write(os, true);
#ifdef PRINT_TEST_INFO
  os << "stations: " << in.V_num << endl;
  os << "vehicles: " << in.L_num << endl;
  os << "maxTime: " << in.that_max << endl;
  os << "steps: " << K << endl;
  os << "minimalActivity: " << minimalActivity << endl;
#endif
}


BBSSSolution BBSSModel::getSolution_step() const
{
  BBSSSolution sol(&in);
  
  vector<Matrix<IntVarArgs>*> service(K + 1);
  Matrix<IntVarArgs> route(route_v, K + 1, in.L_num);
  // FIXME: smart pointers should be used instead
  for (int k = 0; k <= K; k++)
    service[k] = new Matrix<IntVarArgs>(service_v[k], in.L_num, in.V_num);
    
    
    // we have to fill in the routes together with number of stations per each route
    for (int l = 0; l < in.L_num; l++)
    {
      sol.routes[l].clear();
      int node = route(0, l).val();
      int k = 1;
      while (k <= K)
      {
        node = route(k, l).val();
        if (node == in.V_num + in.d[l])
          break;
        if ((*service[k])(l, node).val() != 0)
        {
          BBSSSolution::Stop s(node, (*service[k])(l, node).val(), 0);
          sol.routes[l].push_back(s);
        }
        else
        {
          cerr << "There is no service instruction at node " << node << endl;
        }
        k++;
      }
      sol.rho[l] = (int)sol.routes[l].size();
    }
  
  // FIXME: smart pointers should be used instead
  for (int k = 0; k <= K; k++)
    delete service[k];
  
  sol.calculateRoutesSup();
  sol.calculateStationVisits();
  
  return sol;
  
}


string BBSSModel::solutionToJson_step() const
{
  ostringstream solution;
  Matrix<IntVarArgs> route(route_v, K + 1, in.L_num);
  vector<Matrix<IntVarArgs>*> service(K + 1);
  // FIXME: smart pointers should be used instead
  for (int k = 0; k <= K; k++)
    service[k] = new Matrix<IntVarArgs>(service_v[k], in.L_num, in.V_num);
    
    solution << "{ \"route\": [";
  for (int l = 0; l < in.L_num; l++)
  {
    if (l == 0)
      solution << "[";
    else
      solution << ", [";
    int k = 0;
    solution << route(k, l).val();
    do
    {
      k++;
      solution << ", " << route(k, l).val();
    }
    while (route(k, l).val() < in.V_num); // until depot
    solution << "]";
  }
  solution << "], \"service\": [";
  for (int l = 0; l < in.L_num; l++)
  {
    if (l == 0)
      solution << "[";
    else
      solution << ", [";
    int k = 0, node = route(k, l).val();
    solution << 0;
    do
    {
      k++;
      node = route(k, l).val();
      if (node < in.V_num)
        solution << ", " << (*service[k])(l, node).val();
      else
        solution << ", 0";
    }
    while (node < in.V_num); // until depot
    solution << "]";
  }
  solution << "] }";
  
  for (int k = 0; k <= K; k++)
    delete service[k];
  
  return solution.str();
}

/******************************
 SUPPORT FOR LNS ENGINE
******************************/

unsigned int BBSSModel::relaxable_vars_step() const
{
  return route_v.size();
}

unsigned int BBSSModel::relax_step(Space* neighbor, unsigned int free)
{
  free = min(free, relaxable_vars_step());
  
  BBSSModel* h_tentative = dynamic_cast<BBSSModel*>(neighbor);
  Matrix<IntVarArgs> tentative_route(h_tentative->route_v, K + 1, in.L_num), route(route_v, K + 1, in.L_num);
  
  vector<pair<int, int> > indexes(route_v.size());
  int l = 0;
  for (int i = 0; i < in.L_num; i++)
    for (int j = 1; j < K; j++)
      indexes[l++] = make_pair(i, j);
  
  random_shuffle(indexes.begin(), indexes.end());
  
  set<pair<int, int> > visits_to_release;
  
  for (int i = 0; i < (int)free; i++)
  {
    pair<int, int> to_release = indexes.back();
    visits_to_release.insert(to_release);
    int node = route(to_release.second, to_release.first).val();
    while (node == in.V_num + in.d[l] && to_release.second < K && to_release.second > 0)
    {
      to_release.second--;
      node = route(to_release.second, to_release.first).val();
      if (node == in.V_num + in.d[l])
      {
        visits_to_release.insert(to_release);
      }
    }
    indexes.pop_back();
  }
  
  for (unsigned int i = 0; i < indexes.size(); i++)
  {
    if (find(visits_to_release.begin(), visits_to_release.end(), indexes[i]) == visits_to_release.end())
    {
      pair<int, int> to_fix = indexes[i];
      //cerr << "Fixing: " << to_fix.first << ' ' << to_fix.second << endl;
      int node = route(to_fix.second, to_fix.first).val();
      rel(*h_tentative, tentative_route(to_fix.second, to_fix.first) == node);
    }
  }
  
  
  //tentative->status();
  //cerr << tentative_route << endl;
  
  return free;
}

/******************************
 SUPPORT FOR ACO ENGINE
 ******************************/

const Gecode::IntVarArray& BBSSModel::vars_step() const
{
  return route_v;
}
