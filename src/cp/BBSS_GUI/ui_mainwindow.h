/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QSlider>
#include <QtGui/QStatusBar>
#include <QtGui/QTableWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "bbssgraphwidget.h"
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSolver_Options;
    QAction *actionRun;
    QAction *actionStop;
    QAction *actionReal_Size;
    QAction *actionZoom_to_Fit;
    QAction *action100;
    QAction *actionZoom_In;
    QAction *actionZoom_Out;
    QAction *actionNext_solution;
    QAction *actionPrevious_solution;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *searchProgress;
    QHBoxLayout *horizontalLayout_4;
    BBSSGraphWidget *graphicsView;
    QGridLayout *gridLayout;
    QCustomPlot *costLoad;
    QCustomPlot *costDeviation;
    QCustomPlot *costWork;
    QCustomPlot *cost;
    QSlider *solutionSlider;
    QGroupBox *searchInformation;
    QHBoxLayout *horizontalLayout;
    QTextEdit *consoleOutput;
    QTableWidget *tableWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuRun;
    QMenu *menuView;
    QMenu *menuZoom;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1024, 768);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionSolver_Options = new QAction(MainWindow);
        actionSolver_Options->setObjectName(QString::fromUtf8("actionSolver_Options"));
        actionRun = new QAction(MainWindow);
        actionRun->setObjectName(QString::fromUtf8("actionRun"));
        actionStop = new QAction(MainWindow);
        actionStop->setObjectName(QString::fromUtf8("actionStop"));
        actionReal_Size = new QAction(MainWindow);
        actionReal_Size->setObjectName(QString::fromUtf8("actionReal_Size"));
        actionZoom_to_Fit = new QAction(MainWindow);
        actionZoom_to_Fit->setObjectName(QString::fromUtf8("actionZoom_to_Fit"));
        action100 = new QAction(MainWindow);
        action100->setObjectName(QString::fromUtf8("action100"));
        actionZoom_In = new QAction(MainWindow);
        actionZoom_In->setObjectName(QString::fromUtf8("actionZoom_In"));
        actionZoom_Out = new QAction(MainWindow);
        actionZoom_Out->setObjectName(QString::fromUtf8("actionZoom_Out"));
        actionNext_solution = new QAction(MainWindow);
        actionNext_solution->setObjectName(QString::fromUtf8("actionNext_solution"));
        actionPrevious_solution = new QAction(MainWindow);
        actionPrevious_solution->setObjectName(QString::fromUtf8("actionPrevious_solution"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        searchProgress = new QGroupBox(centralWidget);
        searchProgress->setObjectName(QString::fromUtf8("searchProgress"));
        horizontalLayout_4 = new QHBoxLayout(searchProgress);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        graphicsView = new BBSSGraphWidget(searchProgress);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));

        horizontalLayout_4->addWidget(graphicsView);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        costLoad = new QCustomPlot(searchProgress);
        costLoad->setObjectName(QString::fromUtf8("costLoad"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(costLoad->sizePolicy().hasHeightForWidth());
        costLoad->setSizePolicy(sizePolicy);

        gridLayout->addWidget(costLoad, 1, 1, 1, 1);

        costDeviation = new QCustomPlot(searchProgress);
        costDeviation->setObjectName(QString::fromUtf8("costDeviation"));
        sizePolicy.setHeightForWidth(costDeviation->sizePolicy().hasHeightForWidth());
        costDeviation->setSizePolicy(sizePolicy);

        gridLayout->addWidget(costDeviation, 1, 0, 1, 1);

        costWork = new QCustomPlot(searchProgress);
        costWork->setObjectName(QString::fromUtf8("costWork"));
        sizePolicy.setHeightForWidth(costWork->sizePolicy().hasHeightForWidth());
        costWork->setSizePolicy(sizePolicy);

        gridLayout->addWidget(costWork, 0, 1, 1, 1);

        cost = new QCustomPlot(searchProgress);
        cost->setObjectName(QString::fromUtf8("cost"));
        sizePolicy.setHeightForWidth(cost->sizePolicy().hasHeightForWidth());
        cost->setSizePolicy(sizePolicy);

        gridLayout->addWidget(cost, 0, 0, 1, 1);


        horizontalLayout_4->addLayout(gridLayout);


        verticalLayout_2->addWidget(searchProgress);

        solutionSlider = new QSlider(centralWidget);
        solutionSlider->setObjectName(QString::fromUtf8("solutionSlider"));
        solutionSlider->setEnabled(false);
        solutionSlider->setOrientation(Qt::Horizontal);

        verticalLayout_2->addWidget(solutionSlider);

        searchInformation = new QGroupBox(centralWidget);
        searchInformation->setObjectName(QString::fromUtf8("searchInformation"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(searchInformation->sizePolicy().hasHeightForWidth());
        searchInformation->setSizePolicy(sizePolicy1);
        searchInformation->setMinimumSize(QSize(0, 30));
        horizontalLayout = new QHBoxLayout(searchInformation);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        consoleOutput = new QTextEdit(searchInformation);
        consoleOutput->setObjectName(QString::fromUtf8("consoleOutput"));
        consoleOutput->setEnabled(true);
        sizePolicy.setHeightForWidth(consoleOutput->sizePolicy().hasHeightForWidth());
        consoleOutput->setSizePolicy(sizePolicy);
        consoleOutput->setMinimumSize(QSize(0, 40));
        consoleOutput->setReadOnly(true);

        horizontalLayout->addWidget(consoleOutput);

        tableWidget = new QTableWidget(searchInformation);
        if (tableWidget->columnCount() < 2)
            tableWidget->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
        tableWidget->setSizePolicy(sizePolicy2);
        tableWidget->setMinimumSize(QSize(220, 161));
        tableWidget->setAlternatingRowColors(true);
        tableWidget->setColumnCount(2);
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        tableWidget->verticalHeader()->setVisible(false);

        horizontalLayout->addWidget(tableWidget, 0, Qt::AlignRight);


        verticalLayout_2->addWidget(searchInformation);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuRun = new QMenu(menuBar);
        menuRun->setObjectName(QString::fromUtf8("menuRun"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        menuZoom = new QMenu(menuView);
        menuZoom->setObjectName(QString::fromUtf8("menuZoom"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuRun->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addSeparator();
        menuFile->addSeparator();
        menuFile->addAction(actionSolver_Options);
        menuRun->addAction(actionRun);
        menuRun->addSeparator();
        menuRun->addAction(actionStop);
        menuView->addAction(menuZoom->menuAction());
        menuView->addAction(actionNext_solution);
        menuView->addAction(actionPrevious_solution);
        menuZoom->addAction(actionZoom_to_Fit);
        menuZoom->addAction(action100);
        menuZoom->addSeparator();
        menuZoom->addAction(actionZoom_In);
        menuZoom->addAction(actionZoom_Out);

        retranslateUi(MainWindow);
        QObject::connect(actionOpen, SIGNAL(triggered()), MainWindow, SLOT(OpenFile()));
        QObject::connect(actionRun, SIGNAL(triggered()), MainWindow, SLOT(RunSolver()));
        QObject::connect(actionSolver_Options, SIGNAL(triggered()), MainWindow, SLOT(ShowSolverOptions()));
        QObject::connect(actionStop, SIGNAL(triggered()), MainWindow, SLOT(StopSolver()));
        QObject::connect(actionZoom_to_Fit, SIGNAL(triggered()), MainWindow, SLOT(zoomToFit()));
        QObject::connect(action100, SIGNAL(triggered()), MainWindow, SLOT(zoomToSize()));
        QObject::connect(actionZoom_In, SIGNAL(triggered()), MainWindow, SLOT(zoomIn()));
        QObject::connect(actionZoom_Out, SIGNAL(triggered()), MainWindow, SLOT(zoomOut()));
        QObject::connect(tableWidget, SIGNAL(cellChanged(int,int)), MainWindow, SLOT(UpdateParameters(int,int)));
        QObject::connect(solutionSlider, SIGNAL(valueChanged(int)), MainWindow, SLOT(ShowSolution(int)));
        QObject::connect(actionNext_solution, SIGNAL(triggered()), MainWindow, SLOT(NextSolution()));
        QObject::connect(actionPrevious_solution, SIGNAL(triggered()), MainWindow, SLOT(PreviousSolution()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0, QApplication::UnicodeUTF8));
        actionOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0, QApplication::UnicodeUTF8));
        actionSolver_Options->setText(QApplication::translate("MainWindow", "Solver Options", 0, QApplication::UnicodeUTF8));
        actionRun->setText(QApplication::translate("MainWindow", "Run Solver", 0, QApplication::UnicodeUTF8));
        actionRun->setShortcut(QApplication::translate("MainWindow", "Ctrl+R", 0, QApplication::UnicodeUTF8));
        actionStop->setText(QApplication::translate("MainWindow", "Stop", 0, QApplication::UnicodeUTF8));
        actionStop->setShortcut(QApplication::translate("MainWindow", "Ctrl+T", 0, QApplication::UnicodeUTF8));
        actionReal_Size->setText(QApplication::translate("MainWindow", "Real Size", 0, QApplication::UnicodeUTF8));
        actionZoom_to_Fit->setText(QApplication::translate("MainWindow", "Zoom to Fit", 0, QApplication::UnicodeUTF8));
        action100->setText(QApplication::translate("MainWindow", "100%", 0, QApplication::UnicodeUTF8));
        actionZoom_In->setText(QApplication::translate("MainWindow", "Zoom In", 0, QApplication::UnicodeUTF8));
        actionZoom_In->setShortcut(QApplication::translate("MainWindow", "Ctrl++", 0, QApplication::UnicodeUTF8));
        actionZoom_Out->setText(QApplication::translate("MainWindow", "Zoom Out", 0, QApplication::UnicodeUTF8));
        actionZoom_Out->setShortcut(QApplication::translate("MainWindow", "Ctrl+-", 0, QApplication::UnicodeUTF8));
        actionNext_solution->setText(QApplication::translate("MainWindow", "Next solution", 0, QApplication::UnicodeUTF8));
        actionNext_solution->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", 0, QApplication::UnicodeUTF8));
        actionPrevious_solution->setText(QApplication::translate("MainWindow", "Previous solution", 0, QApplication::UnicodeUTF8));
        actionPrevious_solution->setShortcut(QApplication::translate("MainWindow", "Ctrl+P", 0, QApplication::UnicodeUTF8));
        searchProgress->setTitle(QApplication::translate("MainWindow", "Search Progress", 0, QApplication::UnicodeUTF8));
        searchInformation->setTitle(QApplication::translate("MainWindow", "Search Information", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "Property", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Value", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuRun->setTitle(QApplication::translate("MainWindow", "Run", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("MainWindow", "View", 0, QApplication::UnicodeUTF8));
        menuZoom->setTitle(QApplication::translate("MainWindow", "Zoom", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
