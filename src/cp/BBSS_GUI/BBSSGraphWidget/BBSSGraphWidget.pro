CONFIG      += plugin
TARGET      = $$qtLibraryTarget(bbssgraphwidgetplugin)
TEMPLATE    = lib

HEADERS     = bbssgraphwidgetplugin.h
SOURCES     = bbssgraphwidgetplugin.cpp
RESOURCES   = icons.qrc
LIBS        += -L.

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

CONFIG += static debug

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS    += target

include(bbssgraphwidget.pri)
