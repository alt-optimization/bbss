HEADERS += bbssgraphwidget.h \
    bbssgraph.h \
    bbssframe.h
SOURCES += bbssgraphwidget.cpp \
    bbssframe.cpp

#BBSS Input/output

INCLUDEPATH += ../../../general ../../../mhlib
HEADERS += ../../../general/BBSSSolution.h ../../../general/BBSSInstance.h
SOURCES += ../../../general/BBSSSolution.C ../../../general/BBSSInstance.C
LIBS += -L../../../mhlib -lmh

#BBSSGraph

INCLUDEPATH += ../GVGraph /opt/local/include /opt/local/include/graphviz
HEADERS +=
SOURCES += bbssgraph.cpp ../GVgraph/gvgraph.cpp
LIBS += -L/opt/local/lib -lcgraph -lgvc -lcdt

#JSON
LIBS += -lqjson

FORMS += \
    bbssframe.ui

