#ifndef BBSSGRAPHWIDGET_H
#define BBSSGRAPHWIDGET_H

#include <QGroupBox>
#include <QGraphicsView>
#include <QTableWidget>
#include "../../../general/BBSSInstance.h"
#include "../../bbss_model.h"
#include "bbssgraph.h"
#include <qjson/parser.h>
#include "bbssframe.h"

class BBSSGraphWidget : public BBSSFrame
{
    Q_OBJECT

public:
    BBSSGraphWidget(QWidget *parent = 0);

public:
    // Core widget methods/properties
    void setInstance(BBSSInstance* in);
    BBSSInstance* instance();
    void setRoutingModelSolution(const BBSSModel *space);
    void setStepModelSolution(const BBSSModel *space);
    void setJSONSolution(const QString& json_sol);

    ~BBSSGraphWidget();
public slots:
    void zoomToFit();
    void zoomToSize();
    void zoomIn();
    void zoomOut();
protected slots:
    void showNodeInfo(QString node_name);

protected:
    BBSSInstance* _instance;
    BBSSGraph graph;
    const BBSSModel* _solution;
    QJson::Parser json_parser;
    qreal graphZoom;
    QString _current_node_info;
    BBSSNode* getGraphNode(int v) const;
    void showRoutingModelNodeInfo(QString node_name);
    void showStepModelNodeInfo(QString node_name);
};

#endif
