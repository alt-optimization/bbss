#ifndef BBSSNODE_H
#define BBSSNODE_H

#include "gvgraph.h"

class BBSSEdge;

class BBSSNode : public GVNode
{
    friend class GVGraph<BBSSNode, BBSSEdge>;
public:
    BBSSNode(AbstractGVGraph *g, const QString& name, qreal p, qreal q, qreal c);
    virtual void render();
    void setService(qreal s);    
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    qreal scale, fontsize;
    int _p, _q, _s, _c;
    QGraphicsRectItem *node_rect, *unbalance_rect;
    QGraphicsLineItem *q_level, *p_level;
    QGraphicsTextItem *unbalance_value_text, *q_value_text,  *node_label;
    bool _has_changed;
};

class BBSSEdge : public GVEdge<BBSSNode>
{
    friend class GVGraph<BBSSNode, BBSSEdge>;
public:
    BBSSEdge(AbstractGVGraph *g, BBSSNode *n1, BBSSNode *n2);
    void setVehicle(int v, bool dummy = false);
    void setTravelingTime(int tt);
    void setLabel(const QString& l);
    virtual void render();
protected:
    ~BBSSEdge();
    qreal scale;
    int _vehicle;
    bool _dummy;
    QString _label;
    int _traveling_time;
    bool _has_changed;
};

class BBSSGraph : public GVGraph<BBSSNode, BBSSEdge>
{
    Q_OBJECT
    friend class BBSSNode;
public:
    BBSSGraph(QString name);
		virtual ~BBSSGraph();
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);     
signals:
    void nodeSelected(QString name);
};


#endif // BBSSNODE_H
