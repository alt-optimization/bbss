#include "bbssgraphwidget.h"

#include <QtCore>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QRegExp>
#include "ui_bbssframe.h"

BBSSGraphWidget::BBSSGraphWidget(QWidget *parent) :
    BBSSFrame(parent), graph("BBSS Graph"), graphZoom(1.0)
{ 
    _instance = 0;
    _solution = 0;
    _current_node_info = "";
    ui->view->setScene(&graph);
    connect(&graph, SIGNAL(nodeSelected(QString)), this, SLOT(showNodeInfo(QString)), Qt::QueuedConnection);
    connect(ui->buttonZoomIn, SIGNAL(clicked()), this, SLOT(zoomIn()));
    connect(ui->buttonZoomOut, SIGNAL(clicked()), this, SLOT(zoomOut()));
    connect(ui->buttonZoomToFit, SIGNAL(clicked()), this, SLOT(zoomToFit()));
}

BBSSGraphWidget::~BBSSGraphWidget()
{
    if (_solution)
        delete _solution;
}

void BBSSGraphWidget::setInstance(BBSSInstance *in)
{
    _instance = in;
    if (_solution)
        delete _solution;
    _solution = 0;
    ui->properties->setRowCount(0);
    graph.clear();
    graph.clearNodes();

    // show the graph in the view
    for (int v = 0; v < _instance->V_num; v++)
    {
        BBSSNode* node = new BBSSNode(&graph, QString("s%1").arg(v), _instance->p[v], _instance->q[v], _instance->C[v]);
        graph.addNode(node);
    }
    for (int d = _instance->V_num; d < _instance->V0_num; d++)
    {
        BBSSNode* node = new BBSSNode(&graph, QString("d%1").arg(d), 0, 0, 0);
        graph.addNode(node);
    }
    for (int v = 0; v < _instance->V0_num; v++)
        for (int w = v + 1; w < _instance->V0_num; w++)
        {
            QString v_name = v < _instance->V_num ? QString("s%1").arg(v) : QString("d%1").arg(v);
            QString w_name = w < _instance->V_num ? QString("s%1").arg(w) : QString("d%1").arg(w);
            BBSSEdge* edge = graph.addEdge(v_name, w_name);
            edge->setTravelingTime(std::max(_instance->getTravelingTime(v, w, 0), _instance->getTravelingTime(w, v, 0)));
        }
    graph.applyLayout("neato");
    graph.clearEdges();
    graph.updateLayout(true);
    ui->view->update();
    _current_node_info = "";
}

BBSSInstance *BBSSGraphWidget::instance()
{
    return _instance;
}

BBSSNode *BBSSGraphWidget::getGraphNode(int v) const
{
    if (v >= _instance->V_num)
        return graph.node(QString("d%1").arg(v));
    else
        return graph.node(QString("s%1").arg(v));
}


void BBSSGraphWidget::setStepModelSolution(const BBSSModel *space)
{
    if (space->failed())
        return;
    if (_solution)
        delete _solution;
    _solution = static_cast<const BBSSModel*>(space->clone());
    ui->costMin->setText(QString("%1").arg(_solution->cost_var.min() / (float)COST_SCALE));
    ui->costMax->setText(QString("%1").arg(_solution->cost_var.max() / (float)COST_SCALE));
    graph.clearEdges();
    QList<BBSSNode*> nodes = graph.nodes();
    Gecode::Matrix<IntVarArgs> route(_solution->route_v, _solution->K + 1, _instance->L_num);
    Gecode::Matrix<IntVarArgs> loadV(_solution->loadV_v, _solution->K + 1, _instance->L_num);
    for (int l = 0; l < _instance->L_num; l++)
    {
        BBSSNode* prev_node, *node;
        prev_node = getGraphNode(route(0, l).val()); // This should be assigned!
        for (int k = 1; k <= _solution->K; k++)
        {
            if (!prev_node)
            {
                if (route(k, l).assigned())
                    prev_node = getGraphNode(route(k, l).val());
                continue;
            }
            if (!route(k, l).assigned())
            {
                prev_node = NULL;
                continue;
            }
            node = getGraphNode(route(k, l).val());
            BBSSEdge* edge = graph.addEdge(prev_node, node);
            edge->setLabel(QString("%1/%2").arg(loadV(k - 1, l).min()).arg(loadV(k - 1, l).max()));
            edge->setVehicle(l);
            prev_node = node;
        }
    }
    std::vector<int> overall_service(nodes.size(), 0);
    for (int k = 1; k < _solution->K; k++)
    {
        Gecode::Matrix<IntVarArgs> service_k(_solution->service_v[k], _instance->L_num, _instance->V_num);
        for (int l = 0; l < _instance->L_num; l++)
            for (int v = 0; v < _instance->V_num; v++)
                if (service_k(l, v).assigned())
                    overall_service[v] -= service_k(l, v).val();
    }
    for (int i = 0; i < _instance->V_num; i++)
    {
        BBSSNode* node = graph.node(QString("s%1").arg(i));
        node->setService(overall_service[i]);
    }
    graph.updateLayout(false);
    graph.render();
    graph.update();
    ui->properties->setRowCount(0);
    if (_current_node_info != "")
        showNodeInfo(_current_node_info);
}

void BBSSGraphWidget::setRoutingModelSolution(const BBSSModel *space)
{
    if (space->failed())
        return;
    if (_solution)
        delete _solution;
    _solution = static_cast<const BBSSModel*>(space->clone());
    ui->costMin->setText(QString("%1").arg(_solution->cost_var.min() / (float)COST_SCALE));
    ui->costMax->setText(QString("%1").arg(_solution->cost_var.max() / (float)COST_SCALE));
    graph.clearEdges();
    QList<BBSSNode*> nodes = graph.nodes();
    for (int i = space->regular_stations_end_index - 1; i >= 0; i--)
    {
        if (space->succ[i].assigned())
        {
            int j = space->succ[i].val();
            int v = space->original_node(i), w = space->original_node(j);
            BBSSNode* node, *succ_node;
            node = getGraphNode(v);
            succ_node = getGraphNode(w);
            BBSSEdge* edge = graph.addEdge(node, succ_node);
            edge->setLabel(QString("%1/%2").arg(space->load[j].min()).arg(space->load[j].max()));
            if (space->vehicle[i].assigned())
                edge->setVehicle(space->vehicle[i].val(), space->vehicle[i].val() == space->dummy());
        }
    }
    std::vector<int> overall_service(nodes.size(), 0);
    for (int i = 0; i < space->service.size(); i++)
        if (space->service[i].assigned())
            overall_service[space->original_node(i)] -= space->service[i].val();
    for (int i = 0; i < _instance->V_num; i++)
    {
        BBSSNode* node = graph.node(QString("s%1").arg(i));
        node->setService(overall_service[i]);
    }
    graph.updateLayout(false);
    graph.render();
    graph.update();
    ui->properties->setRowCount(0);
    if (_current_node_info != "")
        showNodeInfo(_current_node_info);
}

void BBSSGraphWidget::setJSONSolution(const QString &json_sol)
{
    QByteArray ba;
    ba.append(json_sol);
    QVariantMap result = json_parser.parse(ba).toMap();
    graph.clearEdges();
    // first reset the service of all nodes
    QList<BBSSNode*> nodes = graph.nodes();
    for (QList<BBSSNode*>::iterator it = nodes.begin(); it != nodes.end(); it++)
        (*it)->setService(0);

    QVariantMap solution = result["solution"].toMap();
    QList<QVariant> routes = solution["route"].toList(), services = solution["service"].toList();
    int v = 0;
    for (QList<QVariant>::iterator vr = routes.begin(), vs = services.begin(); vr != routes.end(); vr++, vs++, v++)
    {
        QList<QVariant> route = vr->toList(), service = vs->toList();
        BBSSNode* prev_node = 0;
        int prev_node_id = -1;
        for (QList<QVariant>::iterator r = route.begin(), s = service.begin(); r != route.end(); r++, s++)
        {
            int node_id = r->toInt();
            int service = s->toInt();
            BBSSNode* node;
            if (node_id >= _instance->V_num)
                node = graph.node(QString("d%1").arg(node_id));
            else
                node = graph.node(QString("s%1").arg(node_id));
            node->setService(-service);
            if (node && prev_node)
            {
                BBSSEdge *edge = graph.addEdge(prev_node->name(), node->name());
                edge->setVehicle(v);
                edge->setTravelingTime(_instance->getTravelingTime(prev_node_id, node_id, 0));
            }
            prev_node = node;
            prev_node_id = node_id;
        }
    }
    graph.updateLayout(false);
    graph.render();
    graph.update();
}

void BBSSGraphWidget::showNodeInfo(QString node_name)
{
    if (!_solution)
        return;
    _current_node_info = node_name;
    if (routing(_solution->opt.model()))
        showRoutingModelNodeInfo(node_name);
    else if (step(_solution->opt.model()))
        showStepModelNodeInfo(node_name);
}

void BBSSGraphWidget::showRoutingModelNodeInfo(QString node_name)
{
    int node_id;
    QRegExp r("([sd])(\\d+)");
    if (r.indexIn(node_name) == -1)
        return; // string didn't match
    node_id = r.cap(2).toInt();
    ui->properties->setHorizontalHeaderItem(0, new QTableWidgetItem("node"));
    ui->properties->setRowCount(0); // to delete previous QTableWidgetItems

    if (r.cap(1) == "s")
    {
        // regular station
        ui->properties->setRowCount(_solution->opt.revisits());
        for (unsigned int k = 0; k < _solution->opt.revisits(); k++)
        {
            ui->properties->setItem(k, 0, new QTableWidgetItem(QString("%1").arg(_solution->first_visit_to(node_id) + k)));
            std::ostringstream os;
            os << _solution->vehicle[_solution->first_visit_to(node_id) + k];
            ui->properties->setItem(k, 1, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->arrival_time[_solution->first_visit_to(node_id) + k];
            ui->properties->setItem(k, 2, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->load[_solution->first_visit_to(node_id) + k];
            ui->properties->setItem(k, 3, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->service[_solution->first_visit_to(node_id) + k];
            ui->properties->setItem(k, 4, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->nbBikes[_solution->first_visit_to(node_id) + k];
            ui->properties->setItem(k, 5, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            Int::ViewValues<Int::IntView> j(_solution->succ[_solution->first_visit_to(node_id) + k]);
            os << "{" << _solution->U[j.val()];
            for (++j; j(); ++j)
            {
                os << ", " << _solution->U[j.val()];
            }
            os << "} / " << _solution->succ[_solution->first_visit_to(node_id) + k];
            ui->properties->setItem(k, 6, new QTableWidgetItem(QString::fromStdString(os.str())));
        }
    }
    else
    {
        // depot
        ui->properties->setRowCount(2 * (_solution->opt.vehicles() + 1));
        for (unsigned int l = 0; l < _solution->opt.vehicles() + 1; l++)
        {
            ui->properties->setItem(l, 0, new QTableWidgetItem(QString("%1").arg(l)));
            std::ostringstream os;
            os << _solution->vehicle[l];
            ui->properties->setItem(l, 1, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->arrival_time[l];
            ui->properties->setItem(l, 2, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->load[l];
            ui->properties->setItem(l, 3, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->service[l];
            ui->properties->setItem(l, 4, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->nbBikes[l];
            ui->properties->setItem(l, 5, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            Int::ViewValues<Int::IntView> j(_solution->succ[l]);
            os << "{" << _solution->U[j.val()];
            for (++j; j(); ++j)
            {
                os << ", " << _solution->U[j.val()];
            }
            os << "} / " << _solution->succ[l];
            ui->properties->setItem(l, 6, new QTableWidgetItem(QString::fromStdString(os.str())));
        }
        for (unsigned int l = 0; l < _solution->opt.vehicles() + 1; l++)
        {
            int l_shifted = l + _solution->regular_stations_end_index;
            ui->properties->setItem(l + _solution->opt.vehicles() + 1, 0, new QTableWidgetItem(QString("%1").arg(l_shifted)));
            std::ostringstream os;
            os << _solution->vehicle[l_shifted];
            ui->properties->setItem(l + _solution->opt.vehicles() + 1, 1, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->arrival_time[l_shifted];
            ui->properties->setItem(l + _solution->opt.vehicles() + 1, 2, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->load[l_shifted];
            ui->properties->setItem(l + _solution->opt.vehicles() + 1, 3, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->service[l_shifted];
            ui->properties->setItem(l + _solution->opt.vehicles() + 1, 4, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            os << _solution->nbBikes[l_shifted];
            ui->properties->setItem(l + _solution->opt.vehicles() + 1, 5, new QTableWidgetItem(QString::fromStdString(os.str())));
            os.str("");
            Int::ViewValues<Int::IntView> j(_solution->succ[l_shifted]);
            os << "{" << _solution->U[j.val()];
            for (++j; j(); ++j)
            {
                os << ", " << _solution->U[j.val()];
            }
            os << "} / " << _solution->succ[l_shifted];
            ui->properties->setItem(l + _solution->opt.vehicles() + 1, 6, new QTableWidgetItem(QString::fromStdString(os.str())));
        }
    }
}

void BBSSGraphWidget::showStepModelNodeInfo(QString node_name)
{
    int node_id;
    QRegExp r("([sd])(\\d+)");
    if (r.indexIn(node_name) == -1)
        return; // string didn't match
    node_id = r.cap(2).toInt();
    std::vector<std::pair<int, int> > visits_to_node;
    ui->properties->setHorizontalHeaderItem(0, new QTableWidgetItem("step"));
    ui->properties->setRowCount(0); // to delete previous QTableWidgetItems

    Gecode::Matrix<IntVarArgs> route(_solution->route_v, _solution->K + 1, _instance->L_num);
    Gecode::Matrix<IntVarArgs> loadV(_solution->loadV_v, _solution->K + 1, _instance->L_num);
    Gecode::Matrix<IntVarArgs> time(_solution->time_v, _solution->K + 1, _instance->L_num);
    Gecode::Matrix<IntVarArgs> nbBikes(_solution->nbBikes_v, _solution->K + 1, _instance->V0_num);
    for (int l = 0; l < _instance->L_num; l++)
    {
        for (int k = 0; k <= _solution->K; k++)
            if (route(k, l).assigned() && route(k, l).val() == node_id)
                visits_to_node.push_back(std::make_pair(k, l));
    }
    ui->properties->setRowCount(visits_to_node.size());
    int row = 0;
    for (std::vector<std::pair<int, int> >::const_iterator it = visits_to_node.begin(); it != visits_to_node.end(); it++, row++)
    {
        int k = it->first, l = it->second;
        Matrix<IntVarArgs> service_k(_solution->service_v[k], _instance->L_num, _instance->V_num);
        std::ostringstream os;
        ui->properties->setItem(row, 0, new QTableWidgetItem(QString("%1").arg(k)));
        ui->properties->setItem(row, 1, new QTableWidgetItem(QString("%1").arg(l)));
        os << time(k, l);
        ui->properties->setItem(row, 2, new QTableWidgetItem(QString::fromStdString(os.str())));
        os.str("");
        os << loadV(k, l);
        ui->properties->setItem(row, 3, new QTableWidgetItem(QString::fromStdString(os.str())));
        os.str("");
        if (node_id < _instance->V_num)
            os << service_k(l, node_id);
        ui->properties->setItem(row, 4, new QTableWidgetItem(QString::fromStdString(os.str())));
        os.str("");
        os << nbBikes(k, node_id);
        ui->properties->setItem(row, 5, new QTableWidgetItem(QString::fromStdString(os.str())));
        os.str("");
        if (k < _solution->K)
            os << route(k + 1, l);
        ui->properties->setItem(row, 6, new QTableWidgetItem(QString::fromStdString(os.str())));
    }
}

void BBSSGraphWidget::zoomToFit()
{
    qreal newGraphZoom = std::min(qreal(ui->view->rect().width()) / graph.sceneRect().width(), qreal(ui->view->rect().height()) / graph.sceneRect().height());
    ui->view->scale(newGraphZoom / graphZoom, newGraphZoom / graphZoom);
    graphZoom = newGraphZoom;
}

void BBSSGraphWidget::zoomToSize()
{
    qreal newGraphZoom = 1.0;
    ui->view->scale(newGraphZoom / graphZoom, newGraphZoom / graphZoom);
    graphZoom = newGraphZoom;
}

void BBSSGraphWidget::zoomIn()
{
    qreal newGraphZoom = graphZoom / 0.9;
    ui->view->scale(newGraphZoom / graphZoom, newGraphZoom / graphZoom);
    graphZoom = newGraphZoom;
}

void BBSSGraphWidget::zoomOut()
{
    qreal newGraphZoom = graphZoom * 0.9;
    ui->view->scale(newGraphZoom / graphZoom, newGraphZoom / graphZoom);
    graphZoom = newGraphZoom;
}
