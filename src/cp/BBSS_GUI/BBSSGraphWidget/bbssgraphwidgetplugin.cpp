#include "bbssgraphwidget.h"
#include "bbssgraphwidgetplugin.h"

#include <QtPlugin>

BBSSGraphWidgetPlugin::BBSSGraphWidgetPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void BBSSGraphWidgetPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool BBSSGraphWidgetPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *BBSSGraphWidgetPlugin::createWidget(QWidget *parent)
{
    return new BBSSGraphWidget(parent);
}

QString BBSSGraphWidgetPlugin::name() const
{
    return QLatin1String("BBSSGraphWidget");
}

QString BBSSGraphWidgetPlugin::group() const
{
    return QLatin1String("Custom");
}

QIcon BBSSGraphWidgetPlugin::icon() const
{
    return QIcon();
}

QString BBSSGraphWidgetPlugin::toolTip() const
{
    return QLatin1String("Graph visualization widget for the BBSS problem");
}

QString BBSSGraphWidgetPlugin::whatsThis() const
{
    return QLatin1String("A widget for visualizing the structure and the current solution of the BBSS problem.");
}

bool BBSSGraphWidgetPlugin::isContainer() const
{
    return false;
}

QString BBSSGraphWidgetPlugin::domXml() const
{
    return QLatin1String("<widget class=\"BBSSGraphWidget\" name=\"bBSSGraphWidget\">\n</widget>\n");
}

QString BBSSGraphWidgetPlugin::includeFile() const
{
    return QLatin1String("bbssgraphwidget.h");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(bbssgraphwidgetplugin, BBSSGraphWidgetPlugin)
#endif // QT_VERSION < 0x050000
