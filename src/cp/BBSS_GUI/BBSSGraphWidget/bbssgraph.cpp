#include "bbssgraph.h"
#include <QLinearGradient>

BBSSNode::BBSSNode(AbstractGVGraph *g, const QString& name, qreal p, qreal q, qreal c)
    : GVNode(g, name, Box), scale(3.0), node_rect(0), unbalance_value_text(0),
      q_value_text(0), unbalance_rect(0), q_level(0), p_level(0), node_label(0), _has_changed(true), _p(p), _q(q), _c(c), _s(0)
{
    render();
    QRectF bb = boundingRect();
    setNodeHeight(bb.height());
    setNodeWidth(bb.width());
}

void BBSSNode::render()
{
    qreal rect_width = 40.0, rect_height = _c * scale;
    if (!_has_changed)
        return;
    if (!node_rect)
    {
        node_rect = new QGraphicsRectItem(QRectF(QPoint(0.0, 0.0), QSizeF(rect_width, rect_height)), this);
        if (_name.startsWith("d"))
            node_rect->setBrush(QBrush(QColor(0, 0, 0xFF)));
        addToGroup(node_rect);
    }
    QFont bold_font;
    bold_font.setBold(true);
    if (!unbalance_value_text)
    {
        unbalance_value_text = new QGraphicsTextItem(this);
        unbalance_value_text->setDefaultTextColor(QColor(0xFF, 0, 0));
        unbalance_value_text->setFont(bold_font);        
    }       
    if (!q_value_text)
    {
        q_value_text = new QGraphicsTextItem(this);
        q_value_text->setPlainText(QString("%1").arg(_q));
        q_value_text->setFont(bold_font);
        q_value_text->setDefaultTextColor(QColor(0, 0, 0xFF));
        //addToGroup(q_value_text);
    }
    if (!unbalance_rect)
    {
        unbalance_rect = new QGraphicsRectItem(this);
        addToGroup(unbalance_rect);
    }
    int unbalance =  _p + _s - _q;
    unbalance_value_text->setPlainText(QString().sprintf("%+d", unbalance));
    QLinearGradient unbalance_color(QPointF(0.0, 0.0), QPointF(0.0, rect_height));
    unbalance_color.setColorAt(0.0, QColor(0xFF, 0, 0));
    unbalance_color.setColorAt(0.5, QColor(0xFF, 0xFF, 0));
    unbalance_color.setColorAt(1.0, QColor(0xFF, 0, 0));
    unbalance_rect->setBrush(QBrush(unbalance_color));
    if (unbalance < 0)
    {
        qreal h = -unbalance * scale, o = _q * scale;
        unbalance_rect->setRect(QRectF(0.0, rect_height - o, rect_width, h));
        unbalance_value_text->setPos(rect_width + 0.1, rect_height - _q * scale + h / 2.0 - unbalance_value_text->boundingRect().height() / 2.0);
        q_value_text->setPos(rect_width + 0.1, rect_height - _q * scale - q_value_text->boundingRect().height());
    }
    else if (unbalance > 0)
    {
        qreal h = unbalance * scale, o = (_p + _s) * scale;
        unbalance_rect->setRect(QRectF(0.0, rect_height - o, rect_width, h));
        unbalance_value_text->setPos(rect_width + 0.1, rect_height - _q * scale - h / 2.0 - unbalance_value_text->boundingRect().height() / 2.0);
        q_value_text->setPos(rect_width + 0.1, rect_height - _q * scale);
    }
    else // unbalance == 0
    {
        qreal o = _q * scale;
        q_value_text->setPos(rect_width + 0.1, rect_height - o);
        unbalance_rect->setRect(QRectF(0.0, rect_height -  o, rect_width, o));
        unbalance_value_text->setPos(0.0, 0.0);        
        unbalance_rect->setBrush(QBrush(QColor(0, 0xFF, 0)));
        unbalance_value_text->setPlainText("");
    }
    addToGroup(unbalance_value_text);
    if (!q_level)
    {
        q_level = new QGraphicsLineItem(QLineF(QPointF(0.0, rect_height - _q * scale), QPointF(rect_width, rect_height - _q * scale)), this);
        q_level->setPen(QPen(QBrush(QColor(0, 0, 0xFF, 0x88)), scale, Qt::DashLine));
        addToGroup(q_level);
    }
    if (!p_level)
    {
        p_level = new QGraphicsLineItem(QLineF(QPointF(0.0, rect_height - _p * scale), QPointF(rect_width, rect_height - _p * scale)), this);
        p_level->setPen(QPen(QBrush(QColor(0xFF, 0, 0, 0x88)), scale, Qt::DashLine));
        addToGroup(p_level);
    }    
    if (!node_label)
    {
        node_label = new QGraphicsTextItem(node_rect);
        node_label->setPlainText(_name);
        node_label->setPos(QPointF(rect_width / 2.0 - node_label->boundingRect().width() / 2.0, rect_height - node_label->boundingRect().height()));
        if (_name.startsWith("d"))
            node_label->setDefaultTextColor(QColor(0, 0, 0xFF));
        addToGroup(node_label);
    }
    _graph->invalidate(boundingRect());
    _has_changed = false;
}

void BBSSNode::setService(qreal s)
{
    if (s != _s)
    {
        _s = s;
        _has_changed = true;
    }
}

void BBSSNode::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    BBSSGraph* _g = dynamic_cast<BBSSGraph*>(_graph);
    emit _g->nodeSelected(_name);
}

BBSSEdge::BBSSEdge(AbstractGVGraph *g, BBSSNode* n1, BBSSNode* n2)
    : GVEdge<BBSSNode>(g, n1, n2), _vehicle(-1), scale(1), _has_changed(false), _traveling_time(0), _dummy(false)
{}

BBSSEdge::~BBSSEdge()
{
    _graph->invalidate(boundingRect());
}

void BBSSEdge::setVehicle(int v, bool dummy)
{
    _vehicle = v;
    _dummy = dummy;
    _has_changed = true;
}

void BBSSEdge::setTravelingTime(int tt)
{
    _traveling_time = tt;
    _has_changed = true;
    ag::agedgeattr(_edge, "len", QString("%1").arg(points2inches(scale * tt)));
}

void BBSSEdge::setLabel(const QString &l)
{
    _label = l;
    _has_changed = true;
}


void BBSSEdge::render()
{
    if (!_has_changed)
        return;    
    if (_vehicle == -1) // this is a small workaround for not assigned edges
        return;
    QColor colors[10] = {QColor("darkRed"), QColor("darkCyan"), QColor("darkMagenta"),
                          QColor("green"), QColor("darkGreen"), QColor("blue"), QColor("darkBlue"),
                          QColor("cyan"), QColor("magenta"), QColor("red") };
    QGraphicsPathItem* path = new QGraphicsPathItem(_path);
    addToGroup(path);
    QPolygonF arrow_f;
    arrow_f << QPointF(0, 0) << QPointF(-0.5, 1) << QPointF(0.5, 1);
    QPointF offset(_path.pointAtPercent(1.0));
    QMatrix arrow_matrix;
    arrow_matrix.scale(6.0, 6.0); // this is the size for the arrow, in points
    arrow_matrix.rotate(90.0 - _path.angleAtPercent(1.0));
    arrow_f = arrow_matrix.map(arrow_f);
    arrow_f.translate(offset);
    QColor color;
    if (!_dummy)
    {
      color = colors[_vehicle];
      color.setAlphaF(0.3);
    }
    else
    {
      color = QColor("darkGray");
      color.setAlphaF(0.0);
    }
    QGraphicsPolygonItem* arrow = new QGraphicsPolygonItem(arrow_f, 0);
    addToGroup(arrow);
    if (_vehicle >= 0 && _vehicle < 10)
    {
        if (!_dummy)
            path->setPen(QPen(color, 2.0));
        else
            path->setPen(QPen(color, 2.0, Qt::DashLine));
    }
    arrow->setPen(QPen(color, 2.0));
    arrow->setBrush(QBrush(color));
    QGraphicsTextItem* label = new QGraphicsTextItem;
    label->setPlainText(_label);
    label->setPos(_path.pointAtPercent(0.5) + QPointF(0.1, 0.1));
    label->rotate(180.0 - _path.angleAtPercent(0.5));
    label->setDefaultTextColor(color);
    addToGroup(label);
    _graph->invalidate(boundingRect());
    _has_changed = false;
}

BBSSGraph::BBSSGraph(QString name) : GVGraph<BBSSNode, BBSSEdge>(name) {}

BBSSGraph::~BBSSGraph() {}

void BBSSGraph::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    GVGraph<BBSSNode, BBSSEdge>::mousePressEvent(event);
} 

