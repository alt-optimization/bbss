#ifndef BBSSFRAME_H
#define BBSSFRAME_H

#include <QFrame>

namespace Ui {
  class BBSSFrame;
}

class BBSSFrame : public QFrame
{
  Q_OBJECT

public:
  explicit BBSSFrame(QWidget *parent = 0);
  ~BBSSFrame();

protected:
  Ui::BBSSFrame *ui;
};

#endif // BBSSFRAME_H
