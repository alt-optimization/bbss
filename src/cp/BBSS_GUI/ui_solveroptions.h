/********************************************************************************
** Form generated from reading UI file 'solveroptions.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SOLVEROPTIONS_H
#define UI_SOLVEROPTIONS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_SolverOptions
{
public:
    QDialogButtonBox *buttonBox;
    QComboBox *model;
    QLabel *label;
    QLabel *label_3;
    QComboBox *branching;
    QComboBox *revisits;
    QLabel *label_4;

    void setupUi(QDialog *SolverOptions)
    {
        if (SolverOptions->objectName().isEmpty())
            SolverOptions->setObjectName(QString::fromUtf8("SolverOptions"));
        SolverOptions->resize(376, 177);
        buttonBox = new QDialogButtonBox(SolverOptions);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(30, 140, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        model = new QComboBox(SolverOptions);
        model->setObjectName(QString::fromUtf8("model"));
        model->setGeometry(QRect(80, 30, 291, 26));
        label = new QLabel(SolverOptions);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 40, 62, 16));
        label_3 = new QLabel(SolverOptions);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 70, 62, 16));
        branching = new QComboBox(SolverOptions);
        branching->setObjectName(QString::fromUtf8("branching"));
        branching->setGeometry(QRect(80, 60, 291, 26));
        revisits = new QComboBox(SolverOptions);
        revisits->setObjectName(QString::fromUtf8("revisits"));
        revisits->setGeometry(QRect(80, 90, 291, 26));
        label_4 = new QLabel(SolverOptions);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 100, 62, 16));

        retranslateUi(SolverOptions);
        QObject::connect(buttonBox, SIGNAL(accepted()), SolverOptions, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SolverOptions, SLOT(reject()));

        QMetaObject::connectSlotsByName(SolverOptions);
    } // setupUi

    void retranslateUi(QDialog *SolverOptions)
    {
        SolverOptions->setWindowTitle(QApplication::translate("SolverOptions", "Solver Options", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SolverOptions", "Model", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SolverOptions", "Branching", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("SolverOptions", "Revisits", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SolverOptions: public Ui_SolverOptions {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SOLVEROPTIONS_H
