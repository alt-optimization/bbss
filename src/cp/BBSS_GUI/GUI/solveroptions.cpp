#include "solveroptions.h"
#include "ui_solveroptions.h"
#include "mainwindow.h"
#include "BBSS.h"

SolverOptions::SolverOptions(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SolverOptions)
{
    ui->setupUi(this);
    MainWindow* p = static_cast<MainWindow*>(parent);
    int value_selected = p->handler.opt.model(), index = 0;
    for (std::list<Option>::iterator it = p->handler.model_variants.begin(); it != p->handler.model_variants.end(); it++)
    {
        std::string tmp = it->name + "  " + it->description;
        QString label(tmp.c_str());
        ui->model->addItem(label);
        if (it->value == value_selected)
            ui->model->setCurrentIndex(index);
        index++;
    }    
    value_selected = p->handler.opt.branching();
    index = 0;
    for (std::list<Option>::iterator it = p->handler.branching_variants.begin(); it != p->handler.branching_variants.end(); it++)
    {
        std::string tmp = it->name + "  " + it->description;
        QString label(tmp.c_str());
        ui->branching->addItem(label);
        if (it->value == value_selected)
            ui->branching->setCurrentIndex(index);
        index++;
    }
    for (unsigned int i = 0; i < 3; i++)
    {
        QString label(QString("%1").arg(i));
        ui->revisits->addItem(label);
    }
    ui->revisits->setCurrentIndex(p->handler.opt.revisits());
}

SolverOptions::~SolverOptions()
{
    delete ui;
}

void SolverOptions::accept()
{
    MainWindow* p = static_cast<MainWindow*>(parent());
    int index = ui->model->currentIndex();
    std::list<Option>::iterator it;
    for (it = p->handler.model_variants.begin(); index > 0 && it != p->handler.model_variants.end(); it++)
      index--;
    p->handler.opt.model(it->value);    
    index = ui->branching->currentIndex();
    for (it = p->handler.branching_variants.begin(); index > 0 && it != p->handler.branching_variants.end(); it++)
      index--;
    p->handler.opt.branching(it->value);
    p->handler.opt.revisits(ui->revisits->currentIndex());
    QDialog::accept();
}
