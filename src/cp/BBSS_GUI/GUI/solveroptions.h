#ifndef SOLVEROPTIONS_H
#define SOLVEROPTIONS_H

#include <QDialog>

namespace Ui {
class SolverOptions;
}

class SolverOptions : public QDialog
{
    Q_OBJECT
    
public:
    explicit SolverOptions(QWidget *parent = 0);
    ~SolverOptions();

protected slots:
    void accept();
    
private:
    Ui::SolverOptions *ui;
};

#endif // SOLVEROPTIONS_H
