#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QFileInfo>
#include "BBSSInstance.h"
#include <csignal>
#include <cstdlib>
#include <csetjmp>
#include <stdexcept>
#include <QtCore>
#include "BBSS.h"
#include <algorithm>
#include <QSocketNotifier>
#include <QTextBlock>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), instance(0)
{
    ui->setupUi(this);
    ui->actionRun->setEnabled(false);
    ui->actionStop->setEnabled(false);
    handler.PrepareOptions();
    handler.opt.interrupt(false);

    ui->cost->addGraph();
    ui->cost->plotLayout()->insertRow(0);
    ui->cost->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->cost, "Total cost"));
    ui->cost->xAxis->setLabel("Time");
    ui->cost->yAxis->setLabel("Cost");

    ui->costDeviation->addGraph();
    ui->costDeviation->plotLayout()->insertRow(0);
    ui->costDeviation->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->costDeviation, "Deviation"));
    ui->costDeviation->xAxis->setLabel("Time");
    ui->costDeviation->yAxis->setLabel("Cost");

    ui->costWork->addGraph();
    ui->costWork->plotLayout()->insertRow(0);
    ui->costWork->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->costWork, "Work"));    
    ui->costWork->xAxis->setLabel("Time");
    ui->costWork->yAxis->setLabel("Cost");

    ui->costLoad->addGraph();
    ui->costLoad->plotLayout()->insertRow(0);
    ui->costLoad->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->costLoad, "Load"));
    ui->costLoad->xAxis->setLabel("Time");
    ui->costLoad->yAxis->setLabel("Cost");

    ui->solutionSlider->setMinimum(0);
}

MainWindow::~MainWindow()
{
    delete ui;
    if (instance)
        delete instance;
}

jmp_buf env;

void on_sigabrt(int signum)
{
    if (signum == SIGABRT)
        longjmp(env, 1);
}

void MainWindow::OpenFile()
{
    QFileDialog fd(this, QFileDialog::tr("Open File"), "", tr("BBSS Files (*.bbs)"));
    if (fd.exec() == QDialog::Rejected)
        return;
    QString filename = fd.selectedFiles().first();

    try
    {        
        if (instance)
            delete instance;
        instance = new BBSSInstance;
        if (setjmp(env) == 0)
        {
            signal(SIGABRT, &on_sigabrt);
            instance->load(qPrintable(filename));
            BBSSfile.set(qPrintable(filename));
            signal(SIGABRT, SIG_DFL);
        }
        else
        {
            signal(SIGABRT, SIG_DFL);
            throw std::runtime_error(qPrintable("Cannot load instance " +  filename));
        }
        ui->statusBar->showMessage("Loaded instance " + filename + " computing layout");        

        ui->graph->setInstance(instance);
        ui->actionRun->setEnabled(true);
        double unbalance = 0.0;
        for (unsigned int s = 0; s < instance->V_num; s++)
            unbalance += fabs(instance->p[s] - instance->q[s]);
        ui->cost->graph(0)->addData(0.0, unbalance);
        ui->cost->xAxis->setRange(0, 60.0);
        ui->cost->yAxis->setRange(0, unbalance + 5);
        ui->cost->replot();
        ui->cost->update();
        ui->costDeviation->graph(0)->addData(0.0, unbalance);
        ui->costWork->graph(0)->addData(0.0, 0.0);
        ui->costLoad->graph(0)->addData(0.0, 0.0);
        ui->consoleOutput->clear();
        ui->statusBar->showMessage("Done");
    }
    catch (std::runtime_error e)
    {
        ui->statusBar->showMessage(e.what());
    }
}

void MainWindow::ShowInstanceInfo()
{
    QFileInfo instance_fileinfo(QString(BBSSfile.getStringValue()));
    for (unsigned int i = ui->tableWidget->rowCount(); i < 3; i++)
        ui->tableWidget->insertRow(0);
    for (unsigned int i = ui->tableWidget->rowCount(); i > 3; i--)
        ui->tableWidget->removeRow(i);
    QTableWidgetItem* instanceItem = new QTableWidgetItem("Instance");
    instanceItem->setFlags(instanceItem->flags() ^ Qt::ItemIsEditable);
    ui->tableWidget->setItem(0, 0, instanceItem);
    QTableWidgetItem* instanceValueItem = new QTableWidgetItem(instance_fileinfo.fileName());
    instanceValueItem->setFlags(instanceValueItem->flags() ^ Qt::ItemIsEditable);
    ui->tableWidget->setItem(0, 1, instanceValueItem);
    QTableWidgetItem* vehiclesItem = new QTableWidgetItem("Vehicles");
    vehiclesItem->setFlags(vehiclesItem->flags() ^ Qt::ItemIsEditable);
    ui->tableWidget->setItem(1, 0, vehiclesItem);
    QTableWidgetItem* vehiclesValueItem = new QTableWidgetItem(QString("%1").arg(BBSSLnum.getStringValue()));
    vehiclesValueItem->setFlags(vehiclesValueItem->flags() | Qt::ItemIsEditable);
    ui->tableWidget->setItem(1, 1, vehiclesValueItem);
    QTableWidgetItem *budgetItem = new QTableWidgetItem("Time budget");
    budgetItem->setFlags(budgetItem->flags() ^ Qt::ItemIsEditable);
    ui->tableWidget->setItem(2, 0, budgetItem);
    QTableWidgetItem *budgetValueItem = new QTableWidgetItem(QString("%1").arg(BBSSthat.getStringValue()));
    budgetValueItem->setFlags(budgetValueItem->flags() | Qt::ItemIsEditable);
    ui->tableWidget->setItem(2, 1, budgetValueItem);
}

void MainWindow::UpdateParameters(int r, int c)
{
    if (c == 0 || r == 0)
        return;
    QTableWidgetItem* item = ui->tableWidget->item(r, c);
    switch (r)
    {
        case 1: // vehicles
            BBSSLnum.set(item->text().toInt());
            break;
        case 2: // time budget
            BBSSthat.set(item->text().toInt());
            break;
    }
}

void MainWindow::RunSolver()
{
    tmp_file = new QTemporaryFile;
    tmp_file->open();
    handler.opt.out_file(qPrintable(tmp_file->fileName()));
    connect(&readOutputTimer, SIGNAL(timeout()), SLOT(readSolverOutput()));
    ui->consoleOutput->clear();
    ui->cost->clearGraphs();
    ui->cost->addGraph();
    ui->costDeviation->clearGraphs();
    ui->costDeviation->addGraph();
    ui->costWork->clearGraphs();
    ui->costWork->addGraph();
    ui->costLoad->clearGraphs();
    ui->costLoad->addGraph();
    readOutputTimer.start(1000); // poll output data every second
    ui->actionStop->setEnabled(true);
    ui->actionRun->setEnabled(false);
    solver = new SolverThread(handler);
    solver->start();
    ui->statusBar->showMessage("Solver started");
    ui->solutionSlider->setEnabled(false);
}

void MainWindow::_StopSolver()
{
    while (solver->isRunning())
    {
        solver->terminate();
        sleep(1);
    }
    delete solver;
    tmp_file->close();
    delete tmp_file;
    ui->actionRun->setEnabled(true);
    ui->statusBar->showMessage("Solver stopped");    
}

void MainWindow::StopSolver()
{
    readOutputTimer.stop();
    disconnect(this, SLOT(readSolverOutput()));
    QtConcurrent::run(this, &MainWindow::_StopSolver);
    ui->actionStop->setEnabled(false);
    ui->statusBar->showMessage("Solver stopping in progress");
    ui->solutionSlider->setMaximum(ui->consoleOutput->document()->lineCount() - 1);
    ui->solutionSlider->setSliderPosition(ui->consoleOutput->document()->lineCount() - 1);
    ui->solutionSlider->setEnabled(true);
}

void MainWindow::ShowSolverOptions()
{
    SolverOptions so(this);
    so.exec();
}

void MainWindow::readSolverOutput()
{
    QTextStream qin(tmp_file);
    if (qin.atEnd())
        return;
    QString line;
    QVariantMap result;
    while ((line = qin.readLine().trimmed()) != "")
    {
        QByteArray ba;
        ba.append(line);
        result = json_parser.parse(ba).toMap();
        double time = result["time"].toDouble(), cost = result["cost"].toDouble();
        ui->cost->graph(0)->addData(time, cost);
        if (cost > 1.0)
            ui->cost->rescaleAxes();
        else
        {
            ui->cost->xAxis->setRange(0.0, time + time * 0.2);
            ui->cost->yAxis->setRange(std::max(0.0, cost - 0.2 * cost), cost + 0.2 * cost);
        }
        ui->cost->replot();
        ui->costDeviation->graph(0)->addData(time, result["cost_deviation"].toDouble());
        ui->costDeviation->rescaleAxes();
        ui->costDeviation->replot();
        ui->costWork->graph(0)->addData(time, result["cost_work"].toDouble());
        ui->costWork->rescaleAxes();
        ui->costWork->replot();
        ui->costLoad->graph(0)->addData(time, result["cost_load"].toDouble());
        ui->costLoad->rescaleAxes();
        ui->costLoad->replot();
        ui->consoleOutput->append(line);
    }
    ShowSolution(ui->consoleOutput->document()->lineCount() - 1);
}

void MainWindow::NextSolution()
{
    if (ui->solutionSlider->isEnabled())
        ui->solutionSlider->setSliderPosition(ui->solutionSlider->sliderPosition() + 1);
}

void MainWindow::PreviousSolution()
{
    if (ui->solutionSlider->isEnabled())
        ui->solutionSlider->setSliderPosition(ui->solutionSlider->sliderPosition() - 1);
}

void MainWindow::ShowSolution(int s)
{
    ui->graph->setJSONSolution(ui->consoleOutput->document()->findBlockByLineNumber(s).text());
    QTextBlockFormat white;
    white.setBackground(Qt::white);
    QTextBlockFormat yellow;
    yellow.setBackground(Qt::yellow);
    QTextCursor all_text(ui->consoleOutput->document());
    all_text.select(QTextCursor::Document);
    all_text.setBlockFormat(white);
    QTextCursor current_block(ui->consoleOutput->document()->findBlockByLineNumber(s));
    current_block.setBlockFormat(yellow);
    current_block.select(QTextCursor::LineUnderCursor);
    ui->consoleOutput->setTextCursor(current_block);
    current_block.clearSelection();
    ui->consoleOutput->setTextCursor(current_block);
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
  QMainWindow::resizeEvent(e);
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    QMainWindow::closeEvent(e);
    kill(getpid(), SIGINT); // this is to possibly terminate the running solver
}

SolverThread::SolverThread(BBSS& h, QObject *parent)
    : QThread(parent), handler(h)
{ setTerminationEnabled(true); }

void SolverThread::run()
{    
    std::string v(BBSSLnum.getStringValue()), t(BBSSthat.getStringValue()); // this is due to some bugs in mhlib
    const char* dummy_argv[6] = { "dummyexec", "-vehicles", v.c_str(), "-timebudget", t.c_str(), BBSSfile().c_str() };
    handler.ParseCmdLine(6, dummy_argv);
    handler.RunScript();
}
