#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "BBSSInstance.h"
#include "BBSS.h"
#include <QThread>
#include <QTemporaryFile>
#include <qjson/parser.h>
#include "solveroptions.h"
#include <QTimer>

namespace Ui {
class MainWindow;
}

class SolverThread : public QThread
{
    Q_OBJECT
public:
    explicit SolverThread(BBSS& handler, QObject *parent = 0);
protected:
    void run();
    BBSS& handler;
};

class MainWindow : public QMainWindow
{
    friend class SolverOptions;
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
protected slots:
    void OpenFile();
    void RunSolver();
    void ShowInstanceInfo();
    void readSolverOutput();
    void ShowSolverOptions();
    void StopSolver();    
    void UpdateParameters(int r, int c);
    void ShowSolution(int s);
    void NextSolution();
    void PreviousSolution();

protected:
    virtual void resizeEvent(QResizeEvent *);
    virtual void closeEvent(QCloseEvent *);
    void LoadInstance(const QString& filename);
    void _StopSolver();

private:
    Ui::MainWindow *ui;
    BBSSInstance* instance;    
    BBSS handler;
    SolverThread* solver;
    QTemporaryFile* tmp_file;
    QJson::Parser json_parser;
    QTimer readOutputTimer;
};

#endif // MAINWINDOW_H
