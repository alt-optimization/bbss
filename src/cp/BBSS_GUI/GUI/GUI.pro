#-------------------------------------------------
#
# Project created by QtCreator 2013-06-28T16:22:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI
TEMPLATE = app

CONFIG += debug

SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    ../../../general/BBSSInstance.C \
    ../../BBSS.C \
    ../../bbss_model.C \
    ../../gecode-lns/lns.C \
    ../../gecode-lns/meta_lns.C \
    solveroptions.cpp \
    ../../routing_model.C \
    ../../step_model.C \
    ../../channeled_model.C \
    ../GVGraph/gvgraph.cpp


HEADERS  += mainwindow.h \
    qcustomplot.h \
    ../../../general/BBSSInstance.h \
    ../../gecode-lns/lns.h ../../gecode-lns/meta_lns.h ../../gecode-lns/lns_space.h \
    ../../BBSS.h \
    solveroptions.h \
    ../../bbss_model.h \
    ../../branching.h \
    ../GVGraph/gvwrapper.h \
    ../../StepBrancher.h \
    ../../RoutingBranchers.h

INCLUDEPATH += ../.. ../../../general ../../../mhlib .. ../GVGraph /opt/local/include /opt/local/include/graphviz ../BBSSGraphWidget

LIBS += -L../../../mhlib -lmh -L/opt/local/lib -lcgraph -lgvc -lcdt \
    -lgecodesearch -lgecodeint -lgecodefloat -lgecodeset -lgecodekernel -lgecodesupport -lgecodeminimodel -lgecodedriver -lgecodegist -lqjson \
    -L../BBSSGraphWidget -lbbssgraphwidgetplugin

FORMS    += mainwindow.ui \
    solveroptions.ui

OTHER_FILES +=
