//
//  StepBrancher.h
//  ACOCP
//
//  Created by Luca Di Gaspero on 19/02/13.
//  Copyright (c) 2013 Università degli Studi di Udine. All rights reserved.
//

#ifndef StepBrancher__
#define StepBrancher__

#include <gecode/search.hh>
#include <gecode/int.hh>

class BBSSModel;
using namespace Gecode;

class StepBrancher : public Brancher
{
public:
  
  class StepChoice : public Choice
  {
  public:

    enum BranchType { BRANCH_ON_ROUTE, BRANCH_ON_SERVICE };

    int step;
    int vehicle;
    BranchType type;
    int *values;
    int n_values;
    
    StepChoice(const Brancher& b, BranchType type, int s, int v, const std::vector<int>& vals);
    /**  */
    virtual size_t size(void) const;
    /** Add choice to archive. */
    virtual void archive(Archive& e) const;
    ~StepChoice();
  };

  
  /** Checks whether there is any station yet to be assigned, stores vector of alternatives based on domains. */
  virtual bool status(const Space& home) const;

  /** Prints a choice. */
  virtual void print(const Space& home, const Choice&	c, unsigned int a, std::ostream& os) const;

  /** Generates a choice. */
  virtual Choice* choice(Space& home);
  
  /** Retrieves a choice from the archive. */
  virtual Choice* choice(const Space&, Archive& e);
  /** Constructor. */
  StepBrancher(Home home);
  
  /** Copy constructor. */
  StepBrancher(Home home, bool share, StepBrancher& b);
  /** Clone support. */
  virtual Actor* copy(Space& home, bool share);
  
  /** Execute choice. */
  virtual ExecStatus commit(Space& home, const Choice& c, unsigned int a);
  /** Post brancher. */
  static void post(BBSSModel& home);
protected:
  std::pair<int, int> next_visit_to_assign(const BBSSModel& r) const;
};


/** Post function. */
void step_brancher(BBSSModel& home)
{
  if (home.failed())
    return;
  StepBrancher::post(home);
}

StepBrancher::StepChoice::StepChoice(const Brancher& b, BranchType t, int s, int v, const std::vector<int>& vals)
: Choice(b, (unsigned int)vals.size()), step(s), vehicle(v), type(t)
{
  n_values = (int)vals.size();
  values = heap.alloc<int>(n_values);
  for (int i = 0; i < (int)vals.size(); i++)
    values[i] = vals[i];
}

/**  */
size_t StepBrancher::StepChoice::size(void) const
{
  return sizeof(StepChoice) + sizeof(int) * n_values;
}

/** Add choice to archive. */
void StepBrancher::StepChoice::archive(Archive& e) const
{
  Choice::archive(e);
  e << type << step << vehicle << n_values;
  for (int i = 0; i < n_values; i++)
    e << values[i];
}

StepBrancher::StepChoice::~StepChoice()
{
  heap.free<int>(values, n_values);
}

std::pair<int, int> StepBrancher::next_visit_to_assign(const BBSSModel &r) const
{
  Matrix<IntVarArgs> route(r.route_v, r.K + 1, r.in.L_num);
  for (int k = 1; k < r.K; k++)
    for (int l = 0; l < r.in.L_num; l++)
    {
      Matrix<IntVarArgs> service(r.service_v[k], r.in.L_num, r.in.V_num);
      if (!route(k, l).assigned() || (route(k, l).val() < r.in.V_num && !service(l, route(k, l).val()).assigned()))
        return std::make_pair(k, l);
    }
  GECODE_NEVER;
  return std::make_pair(-1, -1);
}


Choice* StepBrancher::choice(Space &home)
{
  //const BBSSInstanceOptions& opt = dynamic_cast<BBSSModel&>(home).opt;
  //const BBSSInstance& in = r.getInstance();
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  std::vector<int> values(0);

  Matrix<IntVarArgs> route(r.route_v, r.K + 1, r.in.L_num);
  Matrix<IntVarArgs> nbBikes(r.nbBikes_v, r.K + 1, r.in.V0_num);

  // set current visit
  std::pair<int, int> visit = next_visit_to_assign(r);
  int k = visit.first, l = visit.second;

	if (route(k, l).assigned())
	{
    int station = route(k, l).val();
    Matrix<IntVarArgs> service(r.service_v[k], r.in.L_num, r.in.V_num);
		// this case arises when the service of the current visit has not been set yet
		std::vector<std::pair<int, int> > weighted_values;
		int current_bikes = nbBikes(k - 1, station).val(), target_bikes = r.in.q[station];
		for (Int::ViewValues<Int::IntView> j(service(l, station)); j(); ++j)
		{
			// in case of ties it should be preferred less activity
			int weight = (COST_SCALE * BBSStaubal()) * fabs(current_bikes - j.val() - target_bikes) + (COST_SCALE * BBSStauload()) * fabs(j.val());
			weighted_values.push_back(std::make_pair(weight, j.val()));
		}
		std::sort(weighted_values.begin(), weighted_values.end());
		for (unsigned int i = 0; i < weighted_values.size(); i++)
			values.push_back(weighted_values[i].second);

		return new StepChoice(*this, StepChoice::BRANCH_ON_SERVICE, k, l, values);
	}
	else // !route(k, l).assigned()
	{
		// this case arises when the visit at the current step has not been set yet
		std::vector<std::pair<int, int> > weighted_values;
    Matrix<IntVarArgs> time(r.time_v, r.K + 1, r.in.L_num);
    Matrix<IntVarArgs> loadV(r.loadV_v, r.K + 1, r.in.L_num);
    //    Matrix<IntVarArgs> service(r.service_v[k - 1], r.in.L_num, r.in.V_num);
    int station = route(k - 1, l).val();
    int current_load = loadV(k - 1, l).val();
		for (Int::ViewValues<Int::IntView> j(route(k, l)); j(); ++j)
		{
			int weight, next_station = j.val();

			int next_bikes = nbBikes(k - 1, next_station).val(), target_bikes = r.in.q[next_station], unbalance = next_bikes - target_bikes;
			int vehicle_capacity = r.in.Z[l];

			if (unbalance > 0)
				// pickup station, i.e., service > 0
				weight = std::min(vehicle_capacity - current_load, unbalance);
			else
				// delivery station, i.e., service < 0
				weight = std::min(current_load, -unbalance);

			weighted_values.push_back(std::make_pair((COST_SCALE * BBSStaubal()) * weight - (COST_SCALE * BBSStauload()) * weight - (COST_SCALE * BBSStauwork()) * r.in.getTravelingTime(station, next_station, 0), next_station));
		}
		std::sort(weighted_values.begin(), weighted_values.end(), std::greater<std::pair<int, int> >());
    for (unsigned int i = 0; i < weighted_values.size(); i++)
			values.push_back(weighted_values[i].second);
		return new StepChoice(*this, StepChoice::BRANCH_ON_ROUTE, k, l, values);
  }
}

void StepBrancher::print(const Space& home, const Choice&	c, unsigned int a, std::ostream& os) const
{
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  const StepChoice& sc = static_cast<const StepChoice&>(c);

  switch (sc.type)
  {
    case StepChoice::BRANCH_ON_ROUTE:
      os << "r[" << sc.step << ", " << sc.vehicle << "]=" << sc.values[a];
      break;
    case StepChoice::BRANCH_ON_SERVICE:
    {
      Matrix<IntVarArgs> route(r.route_v, r.K + 1, r.in.L_num);
      int station = route(sc.step, sc.vehicle).val();
      os << "o[" << sc.step << ", " << sc.vehicle << " @" << station << "]=" << sc.values[a];
      break;
    }
  }
}

bool StepBrancher::status(const Space& home) const
{
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  const BBSSInstance& in = r.getInstance();
  Matrix<IntVarArgs> route(r.route_v, r.K + 1, in.L_num);
  for (int k = 1; k < r.K; k++)
    for (int l = 0; l < in.L_num; l++)
    {
      if (!route(k, l).assigned())
        return true;
      Matrix<IntVarArgs> service_at_k(r.service_v[k], in.L_num, in.V_num);
      int station = route(k, l).val();
      if (station == in.V_num + in.d[l])
        continue;
      if (!service_at_k(l, station).assigned())
        return true;
    }

  return false;
}

/** Retrieves a choice from the archive. */
Choice* StepBrancher::choice(const Space&, Archive& e)
{
  int step, vehicle, n_values;
  StepChoice::BranchType t;
  int t_as_int;
  std::vector<int> values;
  e >> t_as_int >> step >> vehicle >> n_values;
  values.resize(n_values, -1);
  for (int i = 0; i < n_values; i++)
    e >> values[i];
  switch (t_as_int)
  {
    case 0: t = StepChoice::BRANCH_ON_ROUTE; break;
    case 1: t = StepChoice::BRANCH_ON_SERVICE; break;
  }
  return new StepChoice(*this, t, step, vehicle, values);
}

/** Constructor. */
StepBrancher::StepBrancher(Home home)
: Brancher(home)
{}

/** Copy constructor. */
StepBrancher::StepBrancher(Home home, bool share, StepBrancher& b)
: Brancher(home, share, b)
{}

/** Clone support. */
Actor* StepBrancher::copy(Space& home, bool share) {
  return new (home) StepBrancher(home, share, *this);
}

/** Execute choice. */
ExecStatus StepBrancher::commit(Space& home, const Choice& c, unsigned int a)
{
  BBSSModel& r = dynamic_cast<BBSSModel&>(home);
	const BBSSInstance& in = r.getInstance();
  const StepChoice& sc = static_cast<const StepChoice&>(c);
  //  std::cerr << "Committing choice " << a << " " << (sc.branch_on_route ? "route" : "service") << "(" << sc.step << ", " << sc.vehicle << ") == " << sc.values[a] << std::endl;

  Matrix<IntVarArgs> route(r.route_v, r.K + 1, in.L_num);

  switch (sc.type)
  {
    case StepChoice::BRANCH_ON_ROUTE:
      return me_failed(Int::IntView(route(sc.step, sc.vehicle)).eq(r, sc.values[a])) ? ES_FAILED : ES_OK;
    case StepChoice::BRANCH_ON_SERVICE:
    {
      Matrix<IntVarArgs> service_at_k(r.service_v[sc.step], in.L_num, in.V_num);
      if (!route(sc.step, sc.vehicle).assigned()) // it handles some subtle cases in which there is a strange relationship among released stations in LNS
        return ES_FAILED;
      int station = route(sc.step, sc.vehicle).val();
      return me_failed(Int::IntView(service_at_k(sc.vehicle, station)).eq(r, sc.values[a])) ? ES_FAILED : ES_OK;
    }
  }
}

/** Post brancher. */
void StepBrancher::post(BBSSModel& home)
{
  (void) new (home) StepBrancher(home);
}

/************
 * Two steps Step Brancher
 ************/

class TwoStepsStepBrancher : public Brancher
{
public:

  class TwoStepsStepChoice : public Choice
  {
  public:

    enum BranchType { FIX_ONE, FIX_TWO };

    int step;
    int vehicle;
    BranchType type;
    struct Values {
      int current_station;
      int next_station;
      int service;
      int distance;
    };
    Values *values;
    int n_values;

    TwoStepsStepChoice(const Brancher& b, BranchType type, int s, int v, const std::vector<Values>& vals);
    /**  */
    virtual size_t size(void) const;
    /** Add choice to archive. */
    virtual void archive(Archive& e) const;
    ~TwoStepsStepChoice();
  };


  /** Checks whether there is any station yet to be assigned, stores vector of alternatives based on domains. */
  virtual bool status(const Space& home) const;

  /** Prints a choice. */
  virtual void print(const Space& home, const Choice&	c, unsigned int a, std::ostream& os) const;

  /** Generates a choice. */
  virtual Choice* choice(Space& home);

  /** Retrieves a choice from the archive. */
  virtual Choice* choice(const Space&, Archive& e);
  /** Constructor. */
  TwoStepsStepBrancher(Home home);

  /** Copy constructor. */
  TwoStepsStepBrancher(Home home, bool share, TwoStepsStepBrancher& b);
  /** Clone support. */
  virtual Actor* copy(Space& home, bool share);

  /** Execute choice. */
  virtual ExecStatus commit(Space& home, const Choice& c, unsigned int a);
  /** Post brancher. */
  static void post(BBSSModel& home);
protected:
  std::pair<int, int> next_visit_to_assign(const BBSSModel& r) const;
};

bool compare_service(const TwoStepsStepBrancher::TwoStepsStepChoice::Values& v1, const TwoStepsStepBrancher::TwoStepsStepChoice::Values& v2)
{
  if (v1.service != v2.service)
    return v1.service > v2.service;
  else if (v1.current_station != v2.current_station)
    return v1.current_station < v2.current_station;
  else if (v1.next_station != v2.next_station)
    return v1.next_station < v2.next_station;
  else
    return false;
}

/** Post function. */
void two_steps_step_brancher(BBSSModel& home)
{
  if (home.failed())
    return;
  TwoStepsStepBrancher::post(home);
}

TwoStepsStepBrancher::TwoStepsStepChoice::TwoStepsStepChoice(const Brancher& b, BranchType t, int s, int v, const std::vector<TwoStepsStepChoice::Values>& vals)
: Choice(b, (unsigned int)vals.size()), step(s), vehicle(v), type(t)
{
  n_values = (int)vals.size();
  values = heap.alloc<Values>(n_values);
  for (int i = 0; i < (int)vals.size(); i++)
    values[i] = vals[i];
}

/**  */
size_t TwoStepsStepBrancher::TwoStepsStepChoice::size(void) const
{
  return sizeof(TwoStepsStepChoice) + sizeof(Values) * n_values;
}

/** Add choice to archive. */
void TwoStepsStepBrancher::TwoStepsStepChoice::archive(Archive& e) const
{
  Choice::archive(e);
  e << type << step << vehicle << n_values;
  for (int i = 0; i < n_values; i++)
    e << values[i].current_station << values[i].next_station << values[i].service;
}

TwoStepsStepBrancher::TwoStepsStepChoice::~TwoStepsStepChoice()
{
  heap.free<Values>(values, n_values);
}

std::pair<int, int> TwoStepsStepBrancher::next_visit_to_assign(const BBSSModel &r) const
{
  Matrix<IntVarArgs> route(r.route_v, r.K + 1, r.in.L_num);
  for (int k = 1; k < r.K; k++)
    for (int l = 0; l < r.in.L_num; l++)
    {
      Matrix<IntVarArgs> service(r.service_v[k], r.in.L_num, r.in.V_num);
      if (!route(k, l).assigned() || (route(k, l).val() < r.in.V_num && !service(l, route(k, l).val()).assigned()))
        return std::make_pair(k, l);
    }
  GECODE_NEVER;
  return std::make_pair(-1, -1);
}


Choice* TwoStepsStepBrancher::choice(Space &home)
{
  //const BBSSInstanceOptions& opt = dynamic_cast<BBSSModel&>(home).opt;
  //const BBSSInstance& in = r.getInstance();
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  std::vector<TwoStepsStepChoice::Values> values;

  Matrix<IntVarArgs> route(r.route_v, r.K + 1, r.in.L_num);
  Matrix<IntVarArgs> nbBikes(r.nbBikes_v, r.K + 1, r.in.V0_num);

  // set current visit
  std::pair<int, int> visit = next_visit_to_assign(r);
  int k = visit.first, l = visit.second;

  Matrix<IntVarArgs> loadV(r.loadV_v, r.K + 1, r.in.L_num);
  int current_load = loadV(k - 1, l).val();


  if (current_load > 0) // the following step should be a sink station
  {
    Matrix<IntVarArgs> service(r.service_v[k], r.in.L_num, r.in.V_num);
    for (Int::ViewValues<Int::IntView> j(route(k, l)); j(); ++j)
    {
      if (service(l, j.val()).in(-current_load)) // the step k on j.val() allows to go to zero load
      {
        TwoStepsStepChoice::Values v;
        v.current_station = j.val();
        v.next_station = -1;
        v.service = -current_load;
        v.distance =  r.in.getTravelingTime(route(k-1, l).val(), j.val(),0);
        values.push_back(v);
      }
    }
    // TODO: sort the values according to minimum "waste"
    return new TwoStepsStepChoice(*this, TwoStepsStepChoice::FIX_ONE, k, l, values);

  }
  else // the following station should be a source and the subsequent one should be a suitable sink or the vehicle should go back to the depot
  {
    std::vector<std::pair<int, TwoStepsStepChoice::Values> > weighted_values;
    for (Int::ViewValues<Int::IntView> i(route(k, l)); i(); ++i)
    {
      if (i.val() == r.in.V_num + r.in.d[l]) // skip ending depot (here, it will be included later)
        continue;
      Matrix<IntVarArgs> service_current(r.service_v[k], r.in.L_num, r.in.V_num);
      Matrix<IntVarArgs> nbBikes(r.nbBikes_v, r.K + 1, r.in.V0_num);
      /*
      if (service_current(l, i.val()).min() < 0) // skip sinks
        continue;
      */
      for (Int::ViewValues<Int::IntView> j(route(k + 1, l)); j(); ++j)
      {
        if (j.val() == r.in.V_num + r.in.d[l]) // skip ending depot
          continue;
        Matrix<IntVarArgs> service_next(r.service_v[k + 1], r.in.L_num, r.in.V_num);
        /*
        if (service_next(l, j.val()).min() > 0) // skip sources
          continue;
        */
        TwoStepsStepChoice::Values v;
        v.current_station = i.val();
        v.next_station = j.val();
        v.service = std::min(nbBikes(k - 1, i.val()).val() - r.in.q[i.val()], -service_next(l, j.val()).min());
        v.distance =  r.in.getTravelingTime(route(k-1, l).val(), i.val(),0) + r.in.getTravelingTime(i.val(), j.val(), 0);
        values.push_back(v);
      }
    }
    std::sort(values.begin(), values.end(), &compare_service);
    // at least go directly to the depot
    TwoStepsStepChoice::Values v;
    v.current_station = r.in.V_num + r.in.d[l];
    v.next_station = r.in.V_num + r.in.d[l];
    v.service = 0;
    values.push_back(v);
    return new TwoStepsStepChoice(*this, TwoStepsStepChoice::FIX_TWO, k, l, values);
  }
}

void TwoStepsStepBrancher::print(const Space& home, const Choice&	c, unsigned int a, std::ostream& os) const
{
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  const TwoStepsStepChoice& sc = static_cast<const TwoStepsStepChoice&>(c);

  switch (sc.type)
  {
    case TwoStepsStepChoice::FIX_ONE:
      os << "r[" << sc.step << ", " << sc.vehicle << "]=" << sc.values[a].current_station << ", o=" << sc.values[a].service;
      break;
    case TwoStepsStepChoice::FIX_TWO:
    {
       os << "r[" << sc.step << ", " << sc.vehicle << "]=" << sc.values[a].current_station << "r[" << sc.step + 1 << ", " << sc.vehicle << "]=" << sc.values[a].next_station << ", o=+/-" << sc.values[a].service;
      break;
    }
  }
}

bool TwoStepsStepBrancher::status(const Space& home) const
{
  const BBSSModel& r = dynamic_cast<const BBSSModel&>(home);
  const BBSSInstance& in = r.getInstance();
  Matrix<IntVarArgs> route(r.route_v, r.K + 1, in.L_num);
  for (int k = 1; k < r.K; k++)
    for (int l = 0; l < in.L_num; l++)
    {
      if (!route(k, l).assigned())
        return true;
      /* Matrix<IntVarArgs> service_at_k(r.service_v[k], in.L_num, in.V_num);
      int station = route(k, l).val();
      if (station == in.V_num + in.d[l])
        continue;
      if (!service_at_k(l, station).assigned())
        return true; */
    }

  return false;
}

/** Retrieves a choice from the archive. */
Choice* TwoStepsStepBrancher::choice(const Space&, Archive& e)
{
  int step, vehicle, n_values;
  TwoStepsStepChoice::BranchType t;
  int t_as_int;
  std::vector<TwoStepsStepChoice::Values> values;
  e >> t_as_int >> step >> vehicle >> n_values;
  values.resize(n_values);
  for (int i = 0; i < n_values; i++)
    e >> values[i].current_station >> values[i].next_station >> values[i].service;
  return new TwoStepsStepChoice(*this, t, step, vehicle, values);
}

/** Constructor. */
TwoStepsStepBrancher::TwoStepsStepBrancher(Home home)
: Brancher(home)
{}

/** Copy constructor. */
TwoStepsStepBrancher::TwoStepsStepBrancher(Home home, bool share, TwoStepsStepBrancher& b)
: Brancher(home, share, b)
{}

/** Clone support. */
Actor* TwoStepsStepBrancher::copy(Space& home, bool share) {
  return new (home) TwoStepsStepBrancher(home, share, *this);
}

/** Execute choice. */
ExecStatus TwoStepsStepBrancher::commit(Space& home, const Choice& c, unsigned int a)
{
  BBSSModel& r = dynamic_cast<BBSSModel&>(home);
	const BBSSInstance& in = r.getInstance();
  const TwoStepsStepChoice& sc = static_cast<const TwoStepsStepChoice&>(c);
  //  std::cerr << "Committing choice " << a << " " << (sc.branch_on_route ? "route" : "service") << "(" << sc.step << ", " << sc.vehicle << ") == " << sc.values[a] << std::endl;

  Matrix<IntVarArgs> route(r.route_v, r.K + 1, in.L_num);
  Matrix<IntVarArgs> service_current(r.service_v[sc.step], in.L_num, in.V_num);
  bool failed;

  switch (sc.type)
  {
    case TwoStepsStepChoice::FIX_ONE:
    {
      failed = me_failed(Int::IntView(route(sc.step, sc.vehicle)).eq(r, sc.values[a].current_station));
      if (!failed)
        failed = me_failed(Int::IntView(service_current(sc.vehicle, sc.values[a].current_station)).eq(r, sc.values[a].service));
      return (failed ? ES_FAILED : ES_OK);
    }
    case TwoStepsStepChoice::FIX_TWO:
    {
      Matrix<IntVarArgs> service_next(r.service_v[sc.step + 1], in.L_num, in.V_num);
      failed = me_failed(Int::IntView(route(sc.step, sc.vehicle)).eq(r, sc.values[a].current_station));
      if (!failed && sc.values[a].current_station != in.V_num + in.d[sc.vehicle])
        failed = me_failed(Int::IntView(service_current(sc.vehicle, sc.values[a].current_station)).eq(r, sc.values[a].service));
      if (!failed && sc.values[a].current_station != in.V_num + in.d[sc.vehicle])
        failed = me_failed(Int::IntView(route(sc.step + 1, sc.vehicle)).eq(r, sc.values[a].next_station));
      if (!failed && sc.values[a].current_station != in.V_num + in.d[sc.vehicle])
        failed = me_failed(Int::IntView(service_next(sc.vehicle, sc.values[a].next_station)).eq(r, -sc.values[a].service));
      return (failed ? ES_FAILED : ES_OK);
    }
  }
}

/** Post brancher. */
void TwoStepsStepBrancher::post(BBSSModel& home)
{
  (void) new (home) TwoStepsStepBrancher(home);
}

#endif /* defined(StepBrancher__) */
