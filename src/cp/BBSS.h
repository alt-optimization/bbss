#ifndef _BBSS_H
#define _BBSS_H

#include "bbss_model.h"
#include "gecode-lns/lns.h"
#include <list>

struct Option
{
public:
    Option(int v, const std::string& n, const std::string& d)
        : value(v), name(n), description(d) {}
    int value;
    std::string name;
    std::string description;
};

class BBSS
{
public:
    BBSS();
    void PrepareOptions();
    void ParseCmdLine(int argc, const char *argv[]);
    void RunScript();
    std::list<Option> model_variants;
    std::list<Option> branching_variants;
    BBSSInstanceOptions opt;
};

const int COST_SCALE = 100000; // according to the current setting in BBSSInstance

#endif
