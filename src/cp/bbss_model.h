#ifndef CPBSS_bbss_model_h
#define CPBSS_bbss_model_h

#include <gecode/driver.hh>
#include <gecode/minimodel.hh>
#include <gecode/int.hh>
#include <iostream>

/** LNS base classes for spaces and options */
#include "gecode-lns/lns_space.h"
#include "gecode-lns/lns.h"

#include "gecode-aco/aco_space.h"
#include "gecode-aco/aco.h"
#include "gecode-aco/aco_brancher.h"


/**
 Options specifically for BBSS models. Note that models are chosen by
 a different option (-model) in the default option set, as well as the
 branching heuristic (-branching). This set of options only handles
 the variants.
 */
template <class OptionsBase>
class BBSSOptions : public OptionsBase {
public:
  BBSSOptions(const char* p) : OptionsBase(p),
    _vehicles("-vehicles", "number of vehicles", 1),
    _capacity("-capacity", "capacity of vehicles", 20),
    _timebudget("-timebudget", "time budget in minutes for vehicles", 480),
    _revisits("-revisits", "revisits allowed at stations", 1),
    _heuristic("-heuristic", "which heuristic to use to solve the model", 0),
    _loading_time("-loading-time", "how long does it take to unload/load a bycicle in the station", 0),
    _quadratic_deviation("-quadratic-deviation", "whether the deviation should be squared", false),
    _hierarchical_cost("-hierarchical-cost", "whether the cost should be hierarchical or flattened", false),
    _full_balancing("-full-balancing", "whether we are trying to fully rebalance a subset of stations", false),
    _monotonicity("-monotonicity", "whether is forbidden or not to temporarily unbalance a station", true),
    _only_essential("-only-essential", "whether only the essential constraints must be posted", false),
    _allow_load_at_final_depot("-allow-load-at-final-depot", "whether the vehicle is allowed to be non empty at the end of its route", false)
  {
    OptionsBase::add(_vehicles);
    OptionsBase::add(_capacity);
    OptionsBase::add(_timebudget);
    OptionsBase::add(_revisits);

    // Heuristic
    _heuristic.add(0, "cp", "constraint programming");
    _heuristic.add(1, "lns", "constraint-based LNS");
    _heuristic.add(2, "aco", "ACO-driven constraint programming");

    OptionsBase::add(_heuristic);
    OptionsBase::add(_loading_time);
    OptionsBase::add(_quadratic_deviation);
    OptionsBase::add(_hierarchical_cost);
    OptionsBase::add(_full_balancing);
    OptionsBase::add(_monotonicity);
    OptionsBase::add(_only_essential);
    OptionsBase::add(_allow_load_at_final_depot);
  }

  unsigned int vehicles(void) const { return _vehicles.value(); }
  void vehicles(unsigned int v) { _vehicles.value(v); }

  unsigned int capacity(void) const { return _capacity.value(); }
  void capacity(unsigned int v) { _capacity.value(v); }

  unsigned int timebudget(void) const { return _timebudget.value(); }
  void timebudget(unsigned int v) { _timebudget.value(v); }

  unsigned int revisits(void) const { return _revisits.value(); }
  void revisits(unsigned int v) { _revisits.value(v); }
 
  std::string heuristic(void) const { 
    switch(_heuristic.value())
    {
        case 0:
            return "cp"; 
        case 1:
            return "lns"; 
        case 2:
            return "aco";
        default:
            throw std::logic_error("inexistent heuristic");
    }
  }

  void heuristic (const std::string& v) 
  {
    if (v == "cp")
        _heuristic.value(0);
    else if (v == "lns")
        _heuristic.value(1);
    else if (v == "aco")
        _heuristic.value(2);
    else
        throw std::logic_error("inexistent heuristic");
  }
  
  unsigned int loading_time(void) const { return _loading_time.value(); }
  void loading_time(unsigned int v) { _loading_time.value(v); }
  
  bool quadratic_deviation(void) const { return _quadratic_deviation.value(); }
  void quadratic_deviation (bool v) { _quadratic_deviation.value(v); }
  
  bool hierarchical_cost(void) const { return _hierarchical_cost.value(); }
  void hierarchical_cost (bool v) { _hierarchical_cost.value(v); }
  
  bool full_balancing(void) const { return _full_balancing.value(); }
  void full_balancing (bool v) { _full_balancing.value(v); }
  
  bool monotonicity(void) const { return _monotonicity.value(); }
  void monotonicity (bool v) { _monotonicity.value(v); }

  bool only_essential(void) const { return _only_essential.value(); }
  void only_essential (bool v) { _only_essential.value(v); }

  bool allow_load_at_final_depot(void) const { return _allow_load_at_final_depot.value(); }
  void allow_load_at_final_depot(bool v) { _allow_load_at_final_depot.value(v); }
  
protected:
  
  BBSSOptions(const BBSSOptions& opt)
  : OptionsBase(opt),
    _vehicles(opt._vehicles),
    _capacity(opt._capacity),
    _timebudget(opt._timebudget),
    _revisits(opt._revisits),
    _heuristic(opt._heuristic),
    _loading_time(opt._loading_time),
    _quadratic_deviation(opt._quadratic_deviation),
    _hierarchical_cost(opt._hierarchical_cost),
    _full_balancing(opt._full_balancing),
    _monotonicity(opt._monotonicity),
    _only_essential(opt._only_essential),
    _allow_load_at_final_depot(opt._allow_load_at_final_depot)
  {}
    
  // Common
  Driver::UnsignedIntOption _vehicles;
  Driver::UnsignedIntOption _capacity;
  Driver::UnsignedIntOption _timebudget;
  Driver::UnsignedIntOption _revisits;
  Driver::StringOption _heuristic;
  Driver::UnsignedIntOption _loading_time;
  Driver::BoolOption _quadratic_deviation;
  Driver::BoolOption _hierarchical_cost;
  Driver::BoolOption _full_balancing;
  Driver::BoolOption _monotonicity;
  Driver::BoolOption _only_essential;
  Driver::BoolOption _allow_load_at_final_depot;
};

typedef BBSSOptions<LNSOptions<ACOOptions<InstanceOptions> > > BBSSInstanceOptions;

/** Options for the model as passed from the command line. */
enum ModelOptions
{
  // Model options (STEP_MODEL is default)
  STEP = 1 << 0,
  ROUTING = 1 << 1,
  CHANNELED = 1 << 2
};

enum Branching
{
  DEFAULT_BRANCHING = 1 << 0,
  SMART_BRANCHING = 1 << 1,
  TWO_STEPS_BRANCHING = 1 << 2
};

/** Query methods */
forceinline bool step(const int model) { return model & STEP; }
forceinline bool routing(const int model) { return model & ROUTING; }
forceinline bool channeled(const int model) { return model & CHANNELED; }
forceinline bool smart_branching(const int branching) { return branching & SMART_BRANCHING; }
forceinline bool two_step_branching(const int branching) { return branching & TWO_STEPS_BRANCHING; }
forceinline bool default_branching(const int branching) { return branching & DEFAULT_BRANCHING; }
forceinline bool lns(const std::string& heuristic) { return heuristic == "lns"; }
forceinline bool aco(const std::string& heuristic) { return heuristic == "aco"; }
forceinline bool cp(const std::string& heuristic) { return heuristic == "cp"; }



/** Print model details */
template <typename T>
forceinline void print_details(const BBSSOptions<T>& opt, std::ostream& os = std::cout)
{
  if (step(opt.model()))
    os << "Step model";
  else if(routing(opt.model()))
    os << "Routing model";
  else
	  os << "Channeled model";
  
  os << " w/";
  
  os << " loading time " << opt.loading_time() << " &";
  
  if (opt.quadratic_deviation())
    os << " quadratic deviation &";
  
  if (opt.hierarchical_cost())
    os << " hierarchical cost &";
  
  if (opt.full_balancing())
    os << " requiring full balancing &";
  
  if (opt.monotonicity())
    os << " with monotonicity and";
  else
    os << " without monotonicity and";

  if (smart_branching(opt.branching()))
    os << " smart branching";
  else if (two_step_branching(opt.branching()))
    os << " two step branching";
  else
    os << " default branching";
  
  if (lns(opt.heuristic()))
    os << " solved with LNS";

  if (cp(opt.heuristic()))
    os << " solved with pure CP";

  if (aco(opt.heuristic()))
    os << " solved with ACO";

  os << ", with a fleet of " << opt.vehicles() << " vehicles of capacity " << opt.capacity() << ", using " << opt.revisits() << " visits, and a time budget of " << opt.timebudget() << " minutes" << std::endl;
}

/**************
 UNIQUE MODEL
 **************/

#include "branching.h"

#include "BBSSInstance.h"
#include "BBSSSolution.h"
#include "BBSS.h"

#define DEBUG

class BBSSModel : public DeferredBranchingSpace<MinimizeScript>, public LNSAbstractSpace, public ACOAbstractSpace
{
public:
  // model debugging function
  forceinline void force_check_status()
  {
#ifdef DEBUG
    static unsigned int count = 0;
    bool s = status();
    if (!s)
    {
      std::cerr << "Failed status at: " << count << std::endl;
      GECODE_NEVER
    }
    count++;
#endif
  }

  /******************************
   ROUTING MODEL STRUCTURES
  ******************************/
  
  /** 
   A vector, mapping (preprocessed) nodes to real nodes in the graph. The mapping implements
   the following policy, nodes:
   
     - [0 .. L_num]: depots, one for each L_num vehicles, plus one for the dummy vehicle
     - [L_num+1 .. L_num+V_num*allowed_visits+1]: regular stations, one for each V_num nodes of the graph (possibly multiplicated by the number of allowed revisits)
     - [regular_stations_start_index .. regular_stations_end_index]: equivalent to the above
     - [regular_stations_end_index+1 .. regular_stations_end_index+L_num+1]: end depots
   
   Note: the "dummy vehicle" is one fake vehicle passing through the unvisited nodes, a facility 
   to allow for optionality of visits. 
  */
  static std::vector<int> U;
  
  /** A vector mapping nodes in order of their suitability, for heuristically guiding the search */
  static std::vector<std::pair<double, int> > order;
  
  /** Starting and ending indices for regular stations (i.e. non-deposit nodes) */
  static int regular_stations_start_index, regular_stations_end_index;
  
  /** Cost of the solutioCost of the solution */
  IntVar cost_var, cost_deviation, cost_load, cost_work;
  
  /** Variables modeling the path (as successor nodes) */
  IntVarArray succ;
  
  /** Variables modeling the path (as predecessor nodes) */
  IntVarArray pred;
  
  /** Variables modeling the work to be done at each node (from a vehicle perspective, i.e. service[i] < 0 means unloading the vehicle to the node, service[i] > 0 means loading the vehicle from the node). */
  IntVarArray service;
  
  /** This is simply the absolute value of service. */
  IntVarArray activity;
  
  /** Variables modeling the current number of bikes at a (replicate) of a station */
  IntVarArray nbBikes;
  
  /** Variables modeling the load of the vehicle serving the current station */
  IntVarArray load;
  
  /** Variables modeling the working times. */
  IntVarArray arrival_time;
  
  /** Variables modeling the deviation from target */
  IntVarArray deviation_from_target;

  /** Processing time at each station */
  IntVarArray processing_time;

  /** Variable modeling the identity of the vehicle which serves each node */
  IntVarArray vehicle;

  /** Variable modeling the length of the route for each vehicle */
  IntVarArray length;
  
  int original_unbalance;

  /** Get the index of the original node corresponding to an augmented graph node passed as parameter */
  forceinline int original_node(int i) const
  {
    return U[i];
  }

  /** Get the index of the node representing the first visit of a node of the original graph */
  forceinline int first_visit_to(int i) const
  {
    return regular_stations_start_index + i * opt.revisits();
  }
  
  /** Tells if a node is a revisit or the original node */
  forceinline bool is_revisit(int i) const
  {
    if (opt.revisits() == 1 || i < regular_stations_start_index || i >= regular_stations_end_index)
      return false;
    return (i > first_visit_to(original_node(i)));
  }
  
  /** Pick starting depot of a vehicle */
  forceinline int starting_depot(unsigned int v) const
  {
    return v;
  }
  
  forceinline int ending_depot(unsigned int v) const
  {
    return regular_stations_end_index + v;
  }
  
  forceinline int dummy() const
  {
    return in.L_num;
  }
  
  forceinline int initial_load(unsigned int i) const
  {    
    return in.bhat[i];
  }

  /** facility variable which represents traveling times as a matrix **/
  IntArgs tt_v;

  /******************************
   STEP MODEL STRUCTURES
  ******************************/
  
  /** Maximum number of steps, computed heuristically as bound */
  int K;
  
  /** Routing variables */
  IntVarArray route_v;
  
  /** Service variables
  from a vehicle perspective, i.e. service < 0 means unloading the vehicle to the node, service > 0 means loading the vehicle from the node) */
  std::vector<IntVarArray> service_v;
  IntVarArray loadV_v;
  
  std::vector<IntVarArray> activity_v;
  IntVarArray nbBikes_v;
  
  /** Time variables */
  IntVarArray time_v;
     
  /******************************
   COMMON VARS
  ******************************/
  
  const BBSSInstanceOptions& opt;
  
  /** Instance reference (to grab data about capacities, nodes, etc.) */
  static BBSSInstance in;
  
  /** Variables for printouts */
  static Gecode::Support::Timer t;
  static double cumulated_time, last_print_time, best_solution_time;
  static long best_solution;  
  
  /******************************
   COMMON METHODS
  ******************************/

  /** HELPERS **/

  /* Compute a overall upper bound for worktime */
  int time_upper_bound() const;

  // Upper bound for maximum vehicle capacity (for service and load constraints)
  int max_vehicle_capacity() const;

  // Upper bound for maximum station capacity (for service and load constraints)
  int max_station_capacity() const;
  
  /** Post step model */
  void post_step();

  /** Post redundant constraints of the step model */
  void post_redundant_step() {} // FIXME: to implement
  
  /** Post routing model */
  void post_routing();

  /** Post redundant constraints of the routing model */
  void post_redundant_routing();
  
  /** Post channeling */
  void post_channeling();

  /** Post redundant constraints of the channeling model */
  void post_redundant_channeling() {} // FIXME: to implement
  
  /** Copy step space (from other step space) */
  void update_step(bool share, BBSSModel&);
  
  /** Copy routing space (from other routing space) */
  void update_routing(bool share, BBSSModel&);
  
  /** Branching strategy for step model */
  void tree_search_branching_step();
  
  /** Branching strategy for routing model */
  void tree_search_branching_routing();
  
  /** Branching strategy for channeled model */
  void tree_search_branching_channeling();
  
  /** Branching to explore the neighborhood of a step model solution */
  void neighborhood_branching_step();
  
  /** Branching to explore the neighborhood of a routing model solution */
  void neighborhood_branching_routing();
  
  /** Branching to explore the neighborhood of a channeled model solution */
  void neighborhood_branching_channeling()
  {
    neighborhood_branching_step();
    neighborhood_branching_routing();
  }
  
  /** Branching to find good initial solution for step model */
  void initial_solution_branching_step(unsigned long int restart);
  
  /** Branching to find good initial solution for routing model */
  void initial_solution_branching_routing(unsigned long int restart);
  
  /** Branching to find good initial solution for channeled model */
  void initial_solution_branching_channeling(unsigned long int restart);
  
  /** Print JSON */
  virtual void print_json(std::ostream& os = std::cout) const;

  /** Print routing */
  virtual void print_routing(std::ostream& os = std::cout) const;

  /** Print step */
  virtual void print_step(std::ostream& os = std::cout) const;
  
  /** Print channeling */
  virtual void print_channeling(std::ostream& os = std::cout) const
  {
    print_json(os);
    return;
    print_step(os);
    print_routing(os);
  }
  
  /** Print routing */
  virtual unsigned int relaxable_vars_routing() const;
  
  /** Print step */
  virtual unsigned int relaxable_vars_step() const;
  
  /** Print channeling */
  virtual unsigned int relaxable_vars_channeling() const
  {
    return relaxable_vars_routing() + relaxable_vars_step();
  }
  
  /** Print routing */
  virtual unsigned int relax_routing(Space* neighbor, unsigned int free);
  
  /** Print step */
  virtual unsigned int relax_step(Space* neighbor, unsigned int free);
  
  /** Print channeling */
  virtual unsigned int relax_channeling(Space* neighbor, unsigned int free);
  
  /** Get solution from routing model */
  virtual BBSSSolution getSolution_routing() const;
  
  /** Get solution from step model */
  virtual BBSSSolution getSolution_step() const;
  
  /** Export routing solution to JSON */
  virtual std::string solutionToJson_routing() const;
  
  /** Export step solution to JSON */
  virtual std::string solutionToJson_step() const;
  
public:
  
  /** Default constructor */
  BBSSModel(const BBSSInstanceOptions& o) : opt(o)
  {
    BBSSfile.set(opt.instance());
    BBSSLnum.set(opt.vehicles());
    BBSSthat.set(opt.timebudget());
    BBSSZ.set(opt.capacity());

    // Load instance
    in.load(BBSSfile());

    // Compute all bounds (K for step, ideally something for routing as well)
    compute_bounds();
    
    // Post only step model
    if (step(opt.model()))
    {
      post_step();
    }
    
    // Post only routing model
    else if (routing(opt.model()))
    {
      post_routing();
    }
    
    // Post both models + channeling
    else
    {
      post_step();
      post_routing();
      post_channeling();
    }
  }
  
  /** Copy constructor */
  BBSSModel(bool share, BBSSModel& s) : DeferredBranchingSpace<MinimizeScript>(share, s), K(s.K), opt(s.opt), original_unbalance(s.original_unbalance)
  {
    // Update step model vars
    if (step(opt.model()) || channeled(opt.model()))
      update_step(share, s);
    
    // Update routing model vars
    if (routing(opt.model()) || channeled(opt.model()))
      update_routing(share, s);
  }
  
  /** Compute maximum number of steps / stations */
  void compute_bounds();
  
  /** Clonation */
  virtual Space* copy(bool share)
  {
    return new BBSSModel(share, *this);
  }
  
  /** Cost variable */
  virtual IntVar cost() const
  {
    return cost_var;
  }
  
  /** Post branching */
  void tree_search_branching()
  {
    if (step(opt.model()))
      tree_search_branching_step();
    
    if (routing(opt.model()))
      tree_search_branching_routing();
    
    if (channeled(opt.model()))
      tree_search_branching_channeling();
  }

  /** Instance getter (for branchers) */
  virtual const BBSSInstance& getInstance() const { return in; }
  
  /** Printing */
  virtual void print(std::ostream& os = std::cout) const
  {
    if (step(opt.model()))
      print_step(os);
    
    if (routing(opt.model()))
      print_routing(os);
    
    if (channeled(opt.model()))
      print_channeling(os);
  }
  
  /** Export solution */
  virtual BBSSSolution getSolution() const
  {
    if (step(opt.model()))
      return getSolution_step();
    
    else //if (routing(opt.model()) || channeled(opt.model()))
      return getSolution_routing();
  }
  
  /** Solution in JSON */
  virtual std::string solutionToJson() const
  {
    if (step(opt.model()))
      return solutionToJson_step();
    else
      return solutionToJson_routing();
  }

  
  /******************************
   SUPPORT FOR LNS ENGINE
  ******************************/
  
  /** Post a random branching, e.g. good for finding a random initial solution in LNS */
  void initial_solution_branching(unsigned long int restart)
  {
    if (step(opt.model()))
      initial_solution_branching_step(restart);
    if (routing(opt.model()))
      initial_solution_branching_routing(restart);
    if (channeled(opt.model()))
      initial_solution_branching_channeling(restart);
  }
  
  /** Post a branching for LNS iteration step, the idea is that it should likely find a good solution  */
  void neighborhood_branching()
  {
    if (step(opt.model()))
      neighborhood_branching_step();
    
    else if (routing(opt.model()))
      neighborhood_branching_routing();
    
    else //if (channeled(opt.model()))
      neighborhood_branching_channeling();
  }
  
  
  /** Returns the number of relaxable variables */
  unsigned int relaxable_vars() const
  {
    if (step(opt.model()))
      return relaxable_vars_step();
    
    else if (routing(opt.model()))
      return relaxable_vars_routing();
    
    else //if (channeled(opt.model()))
      return relaxable_vars_channeling();
  }
  
  /* Returns whether the current space is improving w.r.t. s */
  virtual bool improving(const Space& s, bool strict = true)
  {
    const BBSSModel& _s = dynamic_cast<const BBSSModel&>(s);
    if (strict)
      return this->cost().val() < _s.cost().val();
    else
      return this->cost().val() <= _s.cost().val();
  }
  
  /* Constrain current solution cost to improve over the one passed as parameter plus/minus a delta */
  virtual void constrain(const Space& s, bool strict, double delta)
  {
    const BBSSModel& _s = dynamic_cast<const BBSSModel&>(s);
    if (strict)
    {
      if (opt.hierarchical_cost() && _s.cost_deviation.val() > 0)
        rel(*this, this->cost_deviation < _s.cost_deviation.val() + delta);
      else
        rel(*this, this->cost() < _s.cost().val() + delta);
    }
    else
    {
      if (opt.hierarchical_cost() && _s.cost_deviation.val() > 0)
        rel(*this, this->cost_deviation <= _s.cost_deviation.val() + delta);
      else
        rel(*this, this->cost() <= _s.cost().val() + delta);
    }
  }
  
  void constrain(const Space& s)
  {
    const BBSSModel& _s = dynamic_cast<const BBSSModel&>(s);
    
    if (opt.hierarchical_cost() && _s.cost_deviation.val() > 0)
      rel(*this, cost_deviation < _s.cost_deviation);
    else
      rel(*this, cost_var < _s.cost_var);
  
  }
  
  /** Method to generate a relaxed solution (i.e., a neighbor) from the current one (this) */
  virtual unsigned int relax(Space* neighbor, unsigned int free)
  {
    if (step(opt.model()))
      return relax_step(neighbor, free);
    
    else if (routing(opt.model()))
      return relax_routing(neighbor, free);
    
    else //if (channeled(opt.model()))
      return relax_channeling(neighbor, free);
  }
  
  /****
   SUPPORT FOR ACO ENGINE
   ***/
  
  /// The variables to be used to update pheromones
  virtual const Gecode::IntVarArray& vars() const
  {
    if (step(opt.model()))
      return vars_step();
    
    else if (routing(opt.model()))
      return vars_routing();
    
    else // if (channeled...)
      return vars_channeling();
  }
  
  const Gecode::IntVarArray& vars_step() const;
  const Gecode::IntVarArray& vars_routing() const;
  const Gecode::IntVarArray& vars_channeling() const;
  
  /// Quality function, by default inverse of cost
  virtual double quality() const
  {
    return 1.0 / cost_var.val();
  }
  
  /// Post ACO brancher on vars()
  virtual void post_aco_branching()
  {
    if (step(opt.model()))
      return post_aco_branching_step();
    
    else if (routing(opt.model()))
      return post_aco_branching_routing();
    
    else // if (channeled...)
      return post_aco_branching_channeling();
  }
  
  void post_aco_branching_step();
  void post_aco_branching_routing();
  void post_aco_branching_channeling();
  
  /// Checks whether this space is better than another
  virtual bool better_than(const Gecode::Space* o) const
  {
    const BBSSModel* s = dynamic_cast<const BBSSModel*>(o);
    return cost_var.val() < s->cost_var.val();
  }
  
  /// Pass ACO-specific options
  virtual const Gecode::ACOBaseOptions& aco_options() const
  {
    return dynamic_cast<const ACOBaseOptions&>(opt);
  }

};

/****************************************
 CUSTOM LOOKAHEAD CONSTRAINTS FOR ROUTING
*****************************************/

class LookaheadRoutingTime : public Propagator {
protected:
  int i; // the "current" node
  Int::IntView succ_i;
  Int::IntView departing_time_i;
  Int::IntView processing_succ_i;
  Int::IntView vehicle_i;
  Int::IntView that_vehicle_i;
public:
  // posting
  LookaheadRoutingTime(Space& home, int i, Int::IntView, Int::IntView, Int::IntView, Int::IntView, Int::IntView);
  
  static ExecStatus post(Space& home, int i, Int::IntView, Int::IntView, Int::IntView, Int::IntView, Int::IntView);  // disposal
  virtual size_t dispose(Space& home); // copying
  
  LookaheadRoutingTime(Space& home, bool share, LookaheadRoutingTime& p);
  
  virtual Propagator* copy(Space& home, bool share);
  
  // cost computation
  virtual PropCost cost(const Space&, const ModEventDelta&) const;
  // propagation
  virtual ExecStatus propagate(Space& home, const ModEventDelta&);
};

void lookahead_routing_time(Space& home, int i, IntVar, IntVar, IntVar, IntVar, IntVar);

#endif
