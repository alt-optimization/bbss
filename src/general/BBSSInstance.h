/*! \file BBSSInstance.h 
 \brief A class for instances of the BBSS problem.

 For each application one global instance of this class is created. */

#ifndef BBSSINSTANCE_H
#define BBSSINSTANCE_H

#include <string>
#include <vector>
#include <cassert>
#include "mh_param.h"

/** \defgroup param Global BBSS parameters */

/** \ingroup param
 Filename of the BBSS problem instance to be solved.
 */
extern string_param BBSSfile;

/** \ingroup param
 * File name of the solution file to read and use as initial solution.
 */
extern string_param BBSSsolution;

/* Vehicle related parameters. */

/** \ingroup param
 Filename of the vehicle instance to be loaded.
 */
extern string_param BBSSvehicles;

/** \ingroup param
 Number of vehicles.
 */
extern int_param BBSSLnum;

/** \ingroup param
 Vehicle capacity.
 */
extern int_param BBSSZ;

/** \ingroup param
 Vehicle time budget.
 */
extern int_param BBSSthat;

/** \ingroup param
 Vehicle start node (depot).
 */
extern int_param BBSSs;

/** \ingroup param
 Vehicle end node (depot).
 */
extern int_param BBSSd;

/** \ingroup param
 Vehicle initial bike load.
 */
extern int_param BBSSbhat;

/* Further parameters for problem instance. */

/* \ingroup param
 Szenario to be optimized
 -1: dynamic case
 0: static case, monotonicity
 1: general static case
 */
extern int_param BBSScase;

/* \ingroup param
 tau^bal in objective function.
 */
extern double_param BBSStaubal;

/* \ingroup param
 tau^rem in objective function.
 */
extern double_param BBSStaurem;

/* \ingroup param
 tau^load in objective function.
 */
extern double_param BBSStauload;

/* \ingroup param
 tau^work in objective function.
 */
extern double_param BBSStauwork;

/* \ingroup param
 tau^wait in objective function.
 -1 indicates that no waiting is allowed.
 */
extern double_param BBSStauwait;

/* \ingroup param
 * the starting time of this instance
 * given in minutes from midnight.
 */
extern int_param BBSSstartTime;

/** A class for BBSS instances. */
class BBSSInstance {
public:
	/// Real-world start time of optimization for this instance
	time_t startTime;

	/* Station data */
	/// Number of stations.
	int V_num;
	/// Number of depots.
	int O_num;
	/// Total number of nodes (depots plus stations)
	int V0_num;
	/// Station capacities.
	vector<int> C;
	/// Originally available bikes at stations.
	vector<int> p;
	/// Target number of bikes after reorganization in static case.
	vector<int> q;
	/// p-q, i.e., the number of bikes to pick up or deliver in static case.
	vector<int> pq;
	/// Citybike station IDs
	vector<int> id;
	/// number of matrices for traveling times
	int m_cnt;
	/// This is a supporting vector for the matrices of traveling times 't'
	/// It contains the start time of every matrix (at what point in time
	/// the traveling times of the corresponding matrix apply)
	vector<int> stt;
	/// Traveling times including fixed stopping times between all nodes.
	/// ==first index==:  specifies which matrix to use (e.g., use 0 for the first matrix or if
	///                   you have only one matrix). In general this depends on the current time
	///                   in the algorithm.
	/// ==second index==: specifies the station where to start FROM
	/// ==third index==:  specifies the TARGET station
	vector<vector<vector<int> > > t;
	/// Weights that are added to objective value when the respective station is NOT visited in any route.
	vector<int> omega;
	/// false if omega values are 0 for all stations, true if at least one station has omega > 0.
	bool omegaUsed;
	/// Number of considered times for demands in dynamic case.
	int T_num;
	/// Times for demands in dynamic case.
	vector<int> T;
	/// Accumulated Demands in dynamic case. First index is time index, second the station.
	vector<vector<int> > qT;

	/// Bike demands for a specific station. First index is time index, second station.
	vector<vector<int> > bqT;
	/// Slot demands for a specific station. First index is time index, second station.
	vector<vector<int> > sqT;

	/// number of neighbors to be considered when station gets full or empty. Used
	/// for recalculation of expected demand
	int N_num;

	/// e^beta values, if station is full
	vector<vector<double> > e_beta_full;
	/// e^beta values, if station is empty
	vector<vector<double> > e_beta_empty;

	/* Vehicle data */
	/// Number of vehicles.
	int L_num;
	/// Capacities of vehicles.
	vector<int> Z;
	/// Vehicle time budgets.
	vector<int> that;
	/// Maximum of all vehicles' time budgets
	int that_max;
	/// Vehicle start nodes (depot).
	vector<int> s;
	/// Vehicle end nodes (depot).
	vector<int> d;
	/// Vehicles' initial loads.
	vector<int> bhat;

	/// Empty constructor.
	BBSSInstance() :
			startTime(0), V_num(0), O_num(0), V0_num(0), T_num(0), L_num(0), that_max(
					0) {
	}

	/** Normal Construtor.
	 The filename of the instance is passed as parameter.
	 \param fname Filename from where to load the instance
	 */
	BBSSInstance(const string &fname) :
			V0_num(0), L_num(0) {
		load(fname);
	}
	;

	/** Actual initialization.
	 This method loads the BBSS instance from the specified file.
	 \param fname Filename from where to load the instance
	 */
	void load(const string &fname);

	/** Returns true if the station is a pickup station in the static case. */
	bool isPickupStation(int s) {
		return p[s] > q[s];
	}

	/** Returns true if the station is a delivery station in the static case. */
	bool isDeliveryStation(int s) {
		return p[s] < q[s];
	}

	/** \brief Returns the time needed for a vehicle to travel from one station to another
	 *  \param from         the station where to start from
	 *  \param to           the destination station
	 *  \param currentTime  the current time in the algorithm. The appropriate matrix
	 *                      is chosen accordingly, and, if needed, the value of the traveling
	 *                      time is interpolated.
	 *  \return The traveling time needed to get from one station to another considering the current
	 *          time in the algorithm (e.g., it takes longer to travel in the rush hour). If the current time
	 *          is between two starting times of the matrices the return value is interpolated in order to
	 *          fulfill the FIFO property */
	int getTravelingTime(int from, int to, int currentTime) {
		// if only one matrix is given return the traveling time of this matrix
		if (m_cnt == 1) {
			return t[0][from][to];
		}

		// add start time to current time to get correct value for traveling time
		// in the supplementary datastructure we need only the current working time of
		// the driver, because bhat is only in minutes and does not contain the starting time
		currentTime += BBSSstartTime();

		// if less or equals than starting time of first matrix
		if (currentTime <= stt[0])
			return t[0][from][to];

		// if greater or equals than starting time of last matrix
		int lastTravelingTimeIndex = stt.size() - 1;
		if (currentTime >= stt[lastTravelingTimeIndex])
			return t[lastTravelingTimeIndex][from][to];

		// get the correct matrix for the current time
		int idx = this->getMatrixIndex(currentTime);

		// if time is equals starting time of matrix we do not need to interpolate
		if (currentTime == stt[idx])
			return t[idx][from][to];

		return this->interpolate(currentTime, stt[idx], stt[idx + 1],
				t[idx][from][to], t[idx][from][to]);
	}

	void calculateUserDemand() {
		qT.resize(T_num);

		for (int i = 0; i < T_num; i++) {
			qT[i].resize(V_num);
			for (int j = 0; j < V_num; j++) {
				qT[i][j] = bqT[i][j] - sqT[i][j];
			}
		}
	}

private:
	/** \brief Searches in which time region the parameter currentTime is in.
	 *         Time regions are given by the instance vector stt which specifies
	 *         the start time for the matrices defined in t. It is assumed by this function
	 *         that the values of stt are sorted ascending.
	 *  \param currenTime the time for which the corresponding index in the
	 *                    vector stt should be searched for.
	 *  \return If the parameter currentTime is between two values of the vector
	 *          stt, the lower one is returned.<br>
	 *          If the parameter is before the first time, then 0 is returned.<br>
	 *          If the parameter is after the last time, then stt.size() - 1 is returned.<br>
	 *          If the parameter is equals to some value in stt, the corresponding index of
	 *          this element is returned.*/
	int getMatrixIndex(int currentTime) {
		int nStt = this->stt.size();
		int retIdx = 0;

		if (nStt < 2) {
			return retIdx;
		}

		int nextIdx = (int) nStt / 2;

		int upperBound = -1;
		int lowerBound = -1;
		while (true) {
			if (currentTime == this->stt[nextIdx]) {
				return nextIdx;
			} else if (currentTime < this->stt[nextIdx]
					&& (this->stt[nextIdx] < upperBound || upperBound == -1)) {
				upperBound = this->stt[nextIdx];
				nextIdx = max(0, nextIdx - (((int) nextIdx / 2) + 1));
			} else if (currentTime > this->stt[nextIdx]
					&& (this->stt[nextIdx] > lowerBound || lowerBound == -1)) {
				lowerBound = this->stt[nextIdx];
				retIdx = nextIdx;
				nextIdx = min(nStt - 1, nextIdx + ((int) nextIdx / 2) + 1);
			} else {
				return retIdx;
			}
		}

		assert(false);
	}

	/** \brief This function interpolates the traveling time given in parameters lowerTT and upperTT
	 *         to fit the currentTime
	 *  \param currentTime Some point in time between lowerTime and upperTime for which we want to interpolate
	 *  \param lowerTime   The time for which the traveling time lowerTT applies
	 *  \param upperTime   The time for which the traveling time upperTT applies
	 *  \param lowerTT     The traveling time needed when starting at lowerTime
	 *  \param upperTT     The traveling time needed when starting at upperTime
	 *  \return This function returns the linear interpolation for the currentTime */
	int interpolate(int currentTime, int lowerTime, int upperTime, int lowerTT,
			int upperTT) {
		double percentage = (currentTime - lowerTime) / (upperTime - lowerTime);

		return (int) ((1 - percentage) * lowerTT + percentage * upperTT);
	}
};

#endif //BBSSINSTANCE_H
