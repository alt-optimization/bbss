/*! \file BBSSSolution.h 
  \brief A solution to BBSS with various utility methods.
*/

#ifndef BBSSSOLUTION_H
#define BBSSSOLUTION_H

#include <vector>
#include <map>
#include <utility>
#include "BBSSInstance.h"

using namespace std;


/** \ingroup param
 * If set, the feasibility of each created candidate solution
 * is always checked (for debugging only).*
 */
extern bool_param checkfeas;


/** A class for solutions to the BBSS problem.
 */
class BBSSSolution
{
protected:

	/// Pointer to the BBSS instance.
	BBSSInstance *instance;

public:
	/// Number of stations in the routes.
	vector<int> rho;

	/// Essential data of a vehicle's stop at a station.
	struct Stop
	{
		/// stops of the route excluding start and end nodes (depots).
		int r;
		/// Loading/unloading instructions (positive/negative, respectively).
		int y;
		/// Waiting times for prolonged stays.
		int w;
		/// Constructor.
		Stop(int ri=0,int yi=0,int wi=0) :
			r(ri), y(yi), w(wi)
			{}
	};

	/// All vehicle's routes.
	vector< vector<Stop> >routes;


  	/* Supplemental data structures. Only created when needed, not
	   initialized/copied by constructors/copy method. */

	/// Supplemental data for a vehicle's stop at a station.
	struct StopSup
	{
		/// Time of the stop (including possible waiting time).
		int t;
		/// Number of bikes at vehicle after any (un-)loading.
		int b;
		/// Default constructor.
		StopSup() : t(0), b(0)
			{ }
		StopSup(int ti,int bi) : t(ti), b(bi)
			{ }
	};

	/** Supplemental data for all stops at stations plus the end point (depot).
	    Only valid after calling calculateRoutesSup() or calculateStationVisits().
	*/
	mutable vector<vector<StopSup> > routesSup;

	/** A supporting structure containing information for all events of a station 
	    at a specific time. */
	struct StationVisits
	{
		/// The new number of bikes (after any loading/unloading operations).
		int a;	
		/** List of visits with vehicle and stop indices.
		    In the dynamic case the demands also have corresponding entries
		    with vehicle index -1. */
		vector< pair<int,int> > visits;
		/// Default constructor.
		StationVisits() 
			{ a=0; }
	};

	/** A supplemental data structure containing for each station a
	    sorted map storing data for visits of vehicles.
	    Only valid after calling calculateStationVisits().
	*/
	mutable vector<map<int,StationVisits> > stationVisits;

	/** Returns a clone of this solution. */
	BBSSSolution *clone() const
	{ return new BBSSSolution(*this); }

	/** Copy constructor. */
	BBSSSolution(const BBSSSolution &c) :
		instance(c.instance), rho(c.rho), routes(c.routes), 
		routesSup(0), stationVisits(0)
	{ }
  
	/** Normal constructor creates an empty solution. */
	BBSSSolution(BBSSInstance *inst) :
		instance(inst), rho(inst->L_num), routes(inst->L_num)
	{ }
  
	/** Copy solution. Supporting data structures are cleared. */
	void copy(const BBSSSolution &orig)
	{
		instance=orig.instance;
		rho=orig.rho;
		routes=orig.routes;
		clearSupportingDS();
	}

	/** Resets solution to an empty one and clears supporting data structures. */
	void clear()
	{
		for (int l=0;l<instance->L_num;l++)
		{
			rho[l]=0;
			routes[l].clear();
		}
		routesSup.clear();
		stationVisits.clear();
	}
	
	/** Reset all loading instructions to 0. */
	void clearY()
	{
		for (int l=0;l<instance->L_num;l++)
			for (int i=0;i<rho[l];i++)
				routes[l][i].y=0;
	}

	/** Clears the supporting data structures. Needs to be called if the
	    solution is changed. */
	void clearSupportingDS()
	{ 
		routesSup.clear();
		stationVisits.clear();
	}

	/** Writes the solution to an ostream.
	  \param ostr Stream to use.
	  \param detailed:
	  - 0: Just write out solution
	  - 1: Write solution, check feasibility and write objective value
	  - 2: Write out solution in a more detailed form
	  - 3: Write out solution in detailed form + feasibility check and objective value.
	*/
	void write(ostream &ostr,int detailed=0) const;
	
	/** Writes the single route l to an ostream. */
	void writeRoute(ostream &ostr, int l) const;

	/** Writes all information in stationVisits. */
	void writeStationVisits(ostream &ostr) const;

	/** Writes the solution to an ostream according to the specification of BBSS interfaces. */
	void writeCSV(ostream &ostr) const;

	/** Saves solution to a file.
	  \param fname Filename.
	*/
	void save(const char *fname) const;
	
	/** Loads a solution from a file.
	  \param fname Filename.
	*/
	void load(const char *fname);
	
	/** Calculate supplemental data for vehicle routes. */
	void calculateRoutesSup() const;

	/** Calculate for all the stations when they are visited. 
	    Initializes routesSup and initialize stationVisits. */
	void calculateStationVisits() const;

	/** Checks if the time of route l (or all routes if l==-1) is within that[l],
	   ignoring any possibly additional loading times.
	   Does not need any supplemental data structure. */
	bool isFeasibleBasicTimeOfRoutes(int l=-1);

	/** Checks if the time of all routes are feasible, using the supporting data
	   structures and possibly including variable loading times. */
	bool isFeasibleTotalTimeOfRoutes();

	/** Checks the feasibility of a solution in all details.
	    Terminates with an error message in case of an error.
	    @param bTimeSoftConstraint: If set, routes' time limits are
	    considered as soft constraint and the total excess time 
	    is returned.
	    @param bCheckStationsWithoutLoading: If set, making a stop
	    at a station without any loading (i.e. y[i]==0) is considered
	    an error. */
	int checkFeasibility(bool bTimeSoftConstraint=false,
			bool bCheckStationsWithoutLoading=true) const;

	/** Evaluates the solution and returns objective value.
	 * Feasiblility is checked if both checkfeasible and checkfeas() are set*/
	double objectiveValue(bool checkfeasible = true) const;

	/**
	 * Determines the penalty to be added to the objective value for the given station.
	 * Depends on stationVisits data structure.
	 * @param s: Station index
	 */
	double getUnvisitedPenalty(int s) const;

	/** Remove immediately following duplicates of a station in routes.
	    Returns true if solution has changed.
	    @param l: If -1 all routes are processed, otherwise the specified one. */
	bool removeStationDuplicates(int l=-1);

	/** Remove stations where no loading/unloading takes place from routes.
	    Returns true if solution has changed. */
	bool removeStationsWithoutLoading();

	/** Greedily repairs a solution so that no bikes are brought to
	    the end point (depot). Is guaranteed to work only on solutions
	    that are monotonic (i.e., pickups only on pickup stations,
	    deliveries only at delivery stations).
	    Returns true if solution has changed.
	    If the solution has changed, removeStationsWithoutLoading() should
	    be called afterwards. */
	bool avoidBikesAtDepot();

	/** Greedily repairs a tour of a single vehicle l so that no bikes are brought to
		    the end point (depot). Is guaranteed to work only on solutions
		    that are monotonic (i.e., pickups only on pickup stations,
		    deliveries only at delivery stations).
		    Returns true if solution has changed.
		    If the solution has changed, removeStationsWithoutLoading() should
		    be called afterwards. */
	bool avoidBikesAtDepot(int l);

	/** Possibly shortens the given route lpar or all routes if lpar==-1
	    to make it feasible with respect to the allowed total time that.
	 */
	bool shortenRoutesToFeasibility(int lpar=-1);
	/** Returns the set of finally unsatisfied solutions in the
	    provided vector. Requires StationSup to be valid.
	 */
	void getUnsatisfiedStations(vector<int> &unsatisfied);
	/** Returns the final number of bikes at station s.
	    Requires StationSup to be valid.
	 */
	int getFinalBikesAtStation(int s)
	{
		return !stationVisits[s].empty() ?
				(--stationVisits[s].end())->second.a :
				instance->p[s];
	}

	/** Returns the overall number of stops at stations over all vehicles.
	 * Does not depend on supporting data structures. */
	int getSumOfRho();

	/** Returns a vector of vectors to all the entries in stationVisits.
	 * Each entry is a pair with the first part being the time and the second part
	 * the actual StationVisits structure.
	 * Useful to access the events at stations via subsequent indices 0,1,...
	 * Depends on stationVisits.
	 */
	void getStationVisitsVectors(vector<vector<pair<const int,StationVisits> *> >
		&stationVisitsVectors);

	/** Returns a vector with all stations that are visited at least once in the provided parameter.
	 * Depends on stationVisits.
	 * @return true if at least one station is visited.
	 */
	bool getVisitedStations(vector<int> &visitedStations);
};


#endif //BBSSSOLUTION_H

