# BBSS

This repository contains solvers for the Balancing Bike Sharing Systems (BBSS) problem. 

## Obtaining the full repository

This is an umbrella repository composed by multiple sub-repositories. To get everything run

    git submodule init
    git submodule update

(**note**: this will download about 2GB of problem instances, a visualization tool, and two meta-engines for the GECODE constraint system).

## How to build

The build system is based on CMake, files have bene provided to build the various executables in the project. The suggested way to build it is

    cd src
    mkdir build
    cd build
    cmake ..
    make

note that the project has a number of dependencies

* GECODE (we are working with 4.2.*)
* QJson
* Graphviz

also, in order to make the meta-engines work with GECODE, they must be registered as meta-engines, this can be done by patching the `search.hh` source in the GECODE distribution with either of the `hybrid_gecode.patch` you will find in the `src/cp/gecode-*` subdirectories (the meta-engines).

## External code

Some of the code (namely the files in `src/mhlib`) have been kindly provided by the [Algorithms and Data Structures (ADS)](https://www.ads.tuwien.ac.at/w/Arbeitsbereich_f%C3%BCr_Algorithmen_und_Datenstrukturen) research group at TU Wien.